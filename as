[33mcommit 4acee9cd788a367a8321a30c2678ccdffc3fbadc[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: hosseinjafari <hossein.of.j@gmail.com>
Date:   Wed Apr 24 13:23:53 2019 +0430

    fixing bug for fullscreen total result popup

[1mdiff --git a/Team_res_class.py b/Team_res_class.py[m
[1mindex 9a9134a..57d6687 100644[m
[1m--- a/Team_res_class.py[m
[1m+++ b/Team_res_class.py[m
[36m@@ -62,5 +62,5 @@[m [mclass Team_results(Screen):[m
 [m
     def total(self):[m
 [m
[31m-        pop = Popup(title="", content=Label_f(text="Not implemented yet!!"))[m
[32m+[m[32m        pop = Popup(title="", content=Label_f(text="Not implemented yet!!"), size_hint=(.3,.8))[m
         pop.open()[m
\ No newline at end of file[m
[1mdiff --git a/__pycache__/Team_res_class.cpython-36.pyc b/__pycache__/Team_res_class.cpython-36.pyc[m
[1mindex b9543fc..e5e449c 100644[m
Binary files a/__pycache__/Team_res_class.cpython-36.pyc and b/__pycache__/Team_res_class.cpython-36.pyc differ
