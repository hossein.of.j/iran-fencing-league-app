import xlsxwriter
import os
from importers import *


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


def space_reorder(string):

    strr = string.split(" ")
    strr = [i + " " for i in strr]
    strr.reverse()
    return "".join(strr)


def teams_res(e_teams, f_teams, s_teams):
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
    ur = os.path.join(desktop, "Report.xlsx")
    workbook = xlsxwriter.Workbook(ur)
    sheet = workbook.add_worksheet(name="Results")
    sheet.right_to_left()

    rtl = workbook.add_format({'reading_order': 2})

    sheet.set_column('A:A', 30)

    sheet.merge_range(1,0,1,6,"اپه")
    sheet.set_row(1, 15, workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(2, 0, ("نام",), workbook.add_format({"align": "right", "valign": "vcenter"}))
    sheet.write_column(2, 1, ("تعداد بازی",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(2, 2, ("برد",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(2, 3, ("باخت",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(2, 4, ("عدم حضور",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(2, 5, ("امتیاز",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    for i in range(len(e_teams)):
        name =space_reorder(Make_persian(e_teams[i][1]))
        sheet.write_column(3+i,0,(name,),workbook.add_format({"align": "right", "valign": "vcenter"}))
        sheet.write_column(3+i,1,(e_teams[i][2],),workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(3+i,2,(e_teams[i][3],),workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(3+i,3,(e_teams[i][4],),workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(3+i,4,(e_teams[i][5],),workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(3+i,5,(e_teams[i][6],),workbook.add_format({"align": "center", "valign": "vcenter"}))

    s = len(e_teams) + 4
    sheet.merge_range(s, 0, s, 6, "فلوره")
    sheet.set_row(s, 15, workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s+1, 0, ("نام",), workbook.add_format({"align": "right", "valign": "vcenter"}))
    sheet.write_column(s+1, 1, ("تعداد بازی",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s+1, 2, ("برد",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s+1, 3, ("باخت",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s+1, 4, ("عدم حضور",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s+1, 5, ("امتیاز",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    for i in range(len(f_teams)):
        name = space_reorder(Make_persian(f_teams[i][1]))
        sheet.write_column(s+2 + i, 0, (name,), workbook.add_format({"align": "right", "valign": "vcenter"}))
        sheet.write_column(s+2 + i, 1, (f_teams[i][2],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s+2 + i, 2, (f_teams[i][3],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s+2 + i, 3, (f_teams[i][4],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s+2 + i, 4, (f_teams[i][5],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s+2 + i, 5, (f_teams[i][6],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))

    s = len(f_teams) + s + 4
    sheet.merge_range(s, 0, s, 6, "سابر")
    sheet.set_row(s, 15, workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s + 1, 0, ("نام",), workbook.add_format({"align": "right", "valign": "vcenter"}))
    sheet.write_column(s + 1, 1, ("تعداد بازی",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s + 1, 2, ("برد",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s + 1, 3, ("باخت",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s + 1, 4, ("عدم حضور",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    sheet.write_column(s + 1, 5, ("امتیاز",), workbook.add_format({"align": "center", "valign": "vcenter"}))
    for i in range(len(s_teams)):
        name = space_reorder(Make_persian(s_teams[i][1]))
        sheet.write_column(s + 2 + i, 0, (name,),
                           workbook.add_format({"align": "right", "valign": "vcenter"}))
        sheet.write_column(s + 2 + i, 1, (s_teams[i][2],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s + 2 + i, 2, (s_teams[i][3],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s + 2 + i, 3, (s_teams[i][4],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s + 2 + i, 4, (s_teams[i][5],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))
        sheet.write_column(s + 2 + i, 5, (s_teams[i][6],),
                           workbook.add_format({"align": "center", "valign": "vcenter"}))

    workbook.close()
    pass


if __name__ == "__main__":
    e = [[3, 'ﺍﮐﺎﺩﻣﯽ ﺳﺎﺭﯼ', 1, 1, 0, 0, 3], [7, 'ﺷﻬﺪﺍﯼ ﺧﺮﻣﺸﻬﺮ', 1, 0, 1, 0, 1], [1, 'ﺍﮐﺎﺩﻣﯽﺍﻣﯿﺪﻫﺎﯼ ﺗﻬﺮﺍﻥ', 0, 0, 0, 0, 0], [2, 'ﻫﯿﯿﺖ ﺷﻤﺸﯿﺮﺑﺎﺯﯼ ﺍﺭﺩﮐﺎﻥ', 0, 0, 0, 0, 0], [4, 'ﺍﮐﺎﺩﻣﯽ ﺍﻟﺒﺮﺯ', 0, 0, 0, 0, 0], [5, 'ﻓﻮﻻﺩ ﻣﺒﺎﺭﮐﻪ ﺳﭙﺎﻫﺎﻥ', 0, 0, 0, 0, 0], [8, 'ﻫﯿﯿﺖ ﺍﺫﺭﺑﺎﯾﺠﺎﻥ ﺷﺮﻗﯽ', 0, 0, 0, 0, 0], [9, 'ﻫﯿﯿﺖ ﺷﻤﺸﯿﺮﺑﺎﺯﯼ ﻫﻤﺪﺍﻥ', 0, 0, 0, 0, 0], [11, 'ﺍﺗﺮﺍ ﺍﺭﺗﻮﭘﺪ ﺍﺭﻭﻣﯿﻪ', 0, 0, 0, 0, 0]]
    f = [[2, 'ﻫﯿﯿﺖ ﺷﻤﺸﯿﺮﺑﺎﺯﯼ ﺍﺭﺩﮐﺎﻥ', 0, 0, 0, 0, 0], [5, 'ﻓﻮﻻﺩ ﻣﺒﺎﺭﮐﻪ ﺳﭙﺎﻫﺎﻥ', 0, 0, 0, 0, 0], [6, 'ﻫﻮﺍﭘﯿﻤﺎﯾﯽ ﻭﺍﺭﺵ', 0, 0, 0, 0, 0], [7, 'ﺷﻬﺪﺍﯼ ﺧﺮﻣﺸﻬﺮ', 0, 0, 0, 0, 0], [8, 'ﻫﯿﯿﺖ ﺍﺫﺭﺑﺎﯾﺠﺎﻥ ﺷﺮﻗﯽ', 0, 0, 0, 0, 0]]
    s = [[1, 'ﺍﮐﺎﺩﻣﯽ ﺍﻣﯿﺪﻫﺎﯼ ﺗﻬﺮﺍﻥ', 0, 0, 0, 0, 0], [2, 'ﻫﯿﯿﺖ ﺷﻤﺸﯿﺮﺑﺎﺯﯼ ﺍﺭﺩﮐﺎﻥ', 0, 0, 0, 0, 0], [3, 'ﺍﮐﺎﺩﻣﯽ ﺳﺎﺭﯼ', 0, 0, 0, 0, 0], [4, 'ﺍﮐﺎﺩﻣﯽ ﺍﻟﺒﺮﺯ', 0, 0, 0, 0, 0], [5, 'ﻓﻮﻻﺩ ﻣﺒﺎﺭﮐﻪ ﺳﭙﺎﻫﺎﻥ', 0, 0, 0, 0, 0], [6, 'ﻫﻮﺍﭘﯿﻤﺎﯾﯽ ﻭﺍﺭﺵ', 0, 0, 0, 0, 0], [10, 'ﻫﯿﯿﺖ ﺷﻤﺸﯿﺮﺑﺎﺯﯼ ﮔﻠﺴﺘﺎﻥ', 0, 0, 0, 0, 0]]
    teams_res(e,f,s)
