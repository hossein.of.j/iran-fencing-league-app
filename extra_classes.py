from importers import *
import uploader as SITE

def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


class Label_f(Label):
    def __init__(self, *args, **kwargs):
        super(Label_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Button_f(Button):
    def __init__(self, *args, **kwargs):
        super(Button_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Text_f(TextInput):
    max_chars = NumericProperty(30)
    acceptable_chars = u'ابپتثجچح آژئء خدذرزسشصضطظعغفقکگلمنوهیabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];,.":۱۲۳۴۵۶۷۸۹۰\n'
    str = StringProperty()

    def del_all(self):
        self.text = ""
        self.str = ""

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Text_f, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)


class Num_text(TextInput):
    max_chars = NumericProperty(2)
    acceptable_chars = '1234567890'
    sr = StringProperty()

    def del_all(self):
        self.text = ""
        self.sr = ""

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.sr = self.sr + substring
        self.text = Make_persian(self.sr)
        substring = ""
        super(Num_text, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.sr = self.sr[0:len(self.sr) - 1]
        self.text = Make_persian(self.sr)


class Team_show_item(BoxLayout):

    def __init__(self, *args, **kwargs):
        super(Team_show_item, self).__init__(*args, **kwargs)
        self.caller = None

    def press(self):
        if self.caller:
            self.caller.put_team(self.name)
        else:
            print("no caller seted")
    pass


class Team_show_table(BoxLayout):
    # team = StringProperty()

    # def __init__(self, *args, **kwargs):
    #     super(Team_show_table, self).__init__(*args, **kwargs)
    pass


class Team_pop(Popup):
    def __init__(self, caller, *args, **kwargs):
        self.caller = caller
        super(Team_pop, self).__init__(*args, **kwargs)


class Team_select_pop(BoxLayout):
    orientation = 'vertical'
    padding = [30, 10, 30, 10]
    cnt = 0

    def __init__(self, team_list, *args, **kwargs):
        super(Team_select_pop, self).__init__(*args, **kwargs)
        self.teams = team_list

        for team in team_list:
            wid = Team_show_table()
            wid.team_name = team[1]

            self.add_widget(wid)

        self.bx = BoxLayout(spacing=5, size_hint=(.4, .3), pos_hint={'center_x': .5})
        self.btn = Button(text="Finish")
        self.rnd = Button(text="Random")
        self.bx.add_widget(self.btn)
        self.bx.add_widget(self.rnd)
        self.add_widget(self.bx)
        self.btn.bind(on_release=self.fin)
        self.rnd.bind(on_release=self.rand)

    def rand(self, dt):
        items = [i for i in range(1, len(self.teams) + 1)]
        random.shuffle(items)
        self.t_teams = list()
        for i in range(len(items)):
            iit = (items[i], Make_persian(self.teams[i][1]))

            self.t_teams.append(iit)

        self.parent.parent.parent.caller.rearrenge(self.t_teams)
        self.parent.parent.parent.dismiss()

    def fin(self, dt):
        self.t_teams = list()
        if self.cnt == len(self.teams):
            for items in self.children:
                # self.t_teams.append((int(items.ids.tgl.text), items.ids.name.text))
                if type(items) == Team_show_table:
                    iit = (int(items.ids.tgl.text), items.ids.name.text)
                    self.t_teams.append(iit)
            self.parent.parent.parent.caller.rearrenge(self.t_teams)
            self.parent.parent.parent.dismiss()

    def Clck(self, instance):
        if instance.state == "down":
            instance.text = str(self.cnt + 1)
            self.cnt += 1
            for btn in self.children:
                try:
                    if btn.ids.tgl.text == str(self.cnt - 1):
                        btn.ids.tgl.disabled = True
                except:
                    pass
        else:
            instance.text = ""
            self.cnt -= 1
            for btn in self.children:
                try:
                    if btn.ids.tgl.text == str(self.cnt):
                        btn.ids.tgl.disabled = False
                except:
                    pass


class Team_table_item(BoxLayout):

    def __init__(self, no, name, ttl, *args, **kwargs):
        super(Team_table_item, self).__init__(*args, **kwargs)
        self.no = no
        self.name = name
        self.ttl = ttl
        self.L_n = Label_f(text=self.no, color=(0, 0, 0, 1), size_hint_x=0.06)
        self.L_nm = Label_f(text=Make_persian(self.name), color=(0, 0, 0, 1), size_hint_x=0.3)
        self.add_widget(self.L_n)
        self.add_widget(self.L_nm)
        self.tbl = list()

        for i in range(ttl):
            if i == (int(self.no) - 1):
                self.tbl.append(Label(text="#", font_size=self.height * .2, bold=True, color=(0, 0, 0, 1),
                                      size_hint_x=(0.64 / ttl)))
            else:
                self.tbl.append(
                    Label(text="", font_size=self.height * .2, bold=True, color=(0, 0, 0, 1), size_hint_x=(0.64 / ttl)))

            self.add_widget(self.tbl[i])

        self.bind(size=lambda x, y: Clock.schedule_once(lambda x: self.grid_update()),
                  pos=lambda x, y: Clock.schedule_once(lambda x: self.grid_update()))

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        with self.L_n.canvas.before:
            Color(rgba=(0, 0, 0, 1))
            Line(width=1, rectangle=[self.L_n.x, self.L_n.y, self.L_n.width, self.L_n.height])

        with self.L_nm.canvas.before:
            Color(rgba=(0, 0, 0, 1))
            Line(width=1, rectangle=[self.L_nm.x, self.L_nm.y, self.L_nm.width, self.L_nm.height])

        for rect in self.tbl:
            if rect.text == "#":
                with rect.canvas.before:
                    Color(rgba=(0, 0, 0, 1))
                    Rectangle(size=rect.size, pos=rect.pos)
            else:
                with rect.canvas.before:
                    Color(rgba=(0, 0, 0, 1))
                    Line(width=1, rectangle=[rect.x, rect.y, rect.width, rect.height])
        self.L_nm.font_size = 18

    def set_score(self, i, mod, score=45):
        if mod == "v":
            if score == 45:
                self.tbl[i].text = 'V'
            else:
                self.tbl[i].text = 'V/' + str(score)
        elif mod == 'l':
            self.tbl[i].text = str(score)

    def grid_update(self, *args):
        self.L_n.canvas.before.clear()
        with self.L_n.canvas.before:
            Color(rgba=(0, 0, 0, 1))
            Line(width=1, rectangle=[self.L_n.x, self.L_n.y, self.L_n.width, self.L_n.height])

        self.L_nm.canvas.before.clear()
        with self.L_nm.canvas.before:
            Color(rgba=(0, 0, 0, 1))
            Line(width=1, rectangle=[self.L_nm.x, self.L_nm.y, self.L_nm.width, self.L_nm.height])

        for rect in self.tbl:
            rect.canvas.before.clear()
            if rect.text == "#":
                with rect.canvas.before:
                    Color(rgba=(0, 0, 0, 1))
                    Rectangle(size=rect.size, pos=rect.pos)
            else:
                with rect.canvas.before:
                    Color(rgba=(0, 0, 0, 1))
                    Line(width=1, rectangle=[rect.x, rect.y, rect.width, rect.height])


class Game_btn(Button_f):
    game_id = tuple()
    st = StringProperty()

    def __init__(self, game_ids, name, caller, mod="e", status="Null", *args, **kwargs):
        super(Game_btn, self).__init__(*args, **kwargs)
        self.text = name
        self.font_size = 14
        self.caller = caller
        self.bind(on_release=lambda x: self.caller.ppr(self, mod))
        self.game_id = game_ids
        self.mod = mod
        self.chng(status)
        self.background_down = ""
        self.background_normal = ""
        # self.background_color = (.41, .41, .4, 1)

    def chng(self, st):
        x = st
        self.st = st
        if x == "Null":
            self.background_color = (.41, .41, .4, 1)
        elif x == "Running":
            self.background_color = (0, 0, 1, 1)
        elif x == "Finished":
            self.background_color = (.4, 1, .4, 1)
        else:
            self.background_color = (.41, .41, .4, 1)


class TeamFormSheet(BoxLayout):
    def __init__(self, *args, **kwargs):
        super(TeamFormSheet, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

        self.weapon = Label_f(text="اسلحه", color=(0, 0, 0, 1), pos_hint={"center_x": .29, "center_y": .76})
        self.Team_name = Label_f(text="نام تیم ورزشی", color=(0, 0, 0, 1), pos_hint={"center_x": .565, "center_y": .76})
        self.pl_1 = Label_f(text="", color=(0, 0, 0, 1), size_hint=(None, None), font_size=self.width * .1,
                            pos_hint={"right": 0.175, "center_y": .595})
        self.pl_2 = Label_f(text="", color=(0, 0, 0, 1), size_hint=(None, None), font_size=self.width * .1,
                            pos_hint={"right": 0.175, "center_y": .555})
        self.pl_3 = Label_f(text="", color=(0, 0, 0, 1), size_hint=(None, None), font_size=self.width * .1,
                            pos_hint={"right": 0.175, "center_y": .515})
        self.pl_4 = Label_f(text="", color=(0, 0, 0, 1), size_hint=(None, None), font_size=self.width * .1,
                            pos_hint={"right": 0.175, "center_y": .48})
        self.pl_5 = Label_f(text="", color=(0, 0, 0, 1), size_hint=(None, None), font_size=self.width * .1,
                            pos_hint={"right": 0.175, "center_y": .44})

    def _finish_init(self, *dt):
        self.ids.frame.add_widget(self.weapon)
        self.ids.frame.add_widget(self.Team_name)
        self.pl_1.size = self.pl_1.texture_size
        self.pl_2.size = self.pl_2.texture_size
        self.pl_3.size = self.pl_3.texture_size
        self.pl_4.size = self.pl_4.texture_size
        self.pl_5.size = self.pl_5.texture_size
        self.ids.frame.add_widget(self.pl_1)
        self.ids.frame.add_widget(self.pl_2)
        self.ids.frame.add_widget(self.pl_3)
        self.ids.frame.add_widget(self.pl_4)
        self.ids.frame.add_widget(self.pl_5)

    def name_putter(self, weapon, team_name, pl_1, pl_2, pl_3, pl_4="", pl_5=""):
        self.weapon.text = weapon
        self.Team_name.text = team_name
        self.pl_1.text = pl_1
        self.pl_2.text = pl_2
        self.pl_3.text = pl_3
        self.pl_4.text = pl_4
        self.pl_5.text = pl_5


class Team_sheet(BoxLayout):
    mod='e'
    def __init__(self, *args, **kwargs):
        super(Team_sheet, self).__init__(*args, **kwargs)
        self.L_g_s = list()
        self.R_g_s = list()
        self.L_g_ss = list()
        self.R_g_ss = list()
        self.L_ps = list()
        self.R_ps = list()

        for i in range(9):
            self.L_g_s.append(Label(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(20, 20),
                                    pos_hint={'x': 0.45, 'y': (0.424 - i * 0.032)}))
            self.R_g_s.append(Label(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(20, 20),
                                    pos_hint={'right': (1 - 0.44), 'y': (0.424 - i * 0.032)}))
            self.L_g_ss.append(Label(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(20, 20),
                                     pos_hint={'x': 0.38, 'y': (0.424 - i * 0.032)}))
            self.R_g_ss.append(Label(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(20, 20),
                                     pos_hint={'right': (1 - 0.37), 'y': (0.424 - i * 0.032)}))
            self.L_ps.append(Label_f(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(120, 20), font_size=16,
                                     pos_hint={'x': (.12), 'y': (0.424 - i * 0.032)}))
            self.R_ps.append(Label_f(text='', color=(0, 0, 0, 1), size_hint=(None, None), size=(120, 20), font_size=16,
                                     pos_hint={'right': (1 - 0.105), 'y': (0.424 - i * 0.032)}))

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.team_123 = [self.ids['team_name_123'],
                         self.ids['player_1'],
                         self.ids['player_2'],
                         self.ids['player_3'],
                         self.ids['player_r1']]

        self.team_456 = [self.ids['team_name_456'],
                         self.ids['player_4'],
                         self.ids['player_5'],
                         self.ids['player_6'],
                         self.ids['player_r2']]

        self.winner_team = self.ids.winner_team

        for i in range(9):
            self.ids.frame.add_widget(self.L_g_s[i])
            self.ids.frame.add_widget(self.R_g_s[i])
            self.ids.frame.add_widget(self.L_g_ss[i])
            self.ids.frame.add_widget(self.R_g_ss[i])
            self.ids.frame.add_widget(self.L_ps[i])
            self.ids.frame.add_widget(self.R_ps[i])

    def name_putter(self, team_123=["", "", "", "", ""], team_456=["", "", "", "", ""]):
        self.arrange = [[team_123[0],
                         Make_persian(team_123[1][0]),
                         Make_persian(team_123[2][0]),
                         Make_persian(team_123[3][0]),
                         Make_persian(team_123[4][0])],
                        [team_456[0],
                         Make_persian(team_456[1][0]),
                         Make_persian(team_456[2][0]),
                         Make_persian(team_456[3][0]),
                         Make_persian(team_456[4][0])]
                        ]

        self.team_123[0].text = Make_persian(team_123[0])
        self.team_123[1].text = (team_123[1][0])
        self.team_123[2].text = (team_123[2][0])
        self.team_123[3].text = (team_123[3][0])
        self.team_123[4].text = (team_123[4][0])

        self.team_456[0].text = Make_persian(team_456[0])
        self.team_456[1].text = (team_456[1][0])
        self.team_456[2].text = (team_456[2][0])
        self.team_456[3].text = (team_456[3][0])
        self.team_456[4].text = (team_456[4][0])

        self.L_ps[0].text = team_123[3][0]
        self.L_ps[4].text = team_123[3][0]
        self.L_ps[8].text = team_123[3][0]

        self.L_ps[1].text = team_123[1][0]
        self.L_ps[3].text = team_123[1][0]
        self.L_ps[6].text = team_123[1][0]

        self.L_ps[2].text = team_123[2][0]
        self.L_ps[5].text = team_123[2][0]
        self.L_ps[7].text = team_123[2][0]

        self.R_ps[0].text = team_456[3][0]
        self.R_ps[3].text = team_456[3][0]
        self.R_ps[7].text = team_456[3][0]

        self.R_ps[1].text = team_456[2][0]
        self.R_ps[5].text = team_456[2][0]
        self.R_ps[8].text = team_456[2][0]

        self.R_ps[2].text = team_456[1][0]
        self.R_ps[4].text = team_456[1][0]
        self.R_ps[6].text = team_456[1][0]

    def prnt(self,cp):
        PRINT.Team_sheet_pdf_make(self.arrange[0], self.arrange[1], cp, self.mod)
        PRINT.send_to_printer()
        print("Print")

    def prnt_l(self,cp):
        PRINT.Team_sheet_pdf_make(self.arrange[0], self.arrange[1], cp, self.mod)
        PRINT.send_to_printer_l()



class Team_select_popup_call_content(BoxLayout):
    btn_text = ['1', '2', '3', 'R1', '4', '5', '6', 'R2']

    def __init__(self, caller, *args, **kwargs):
        super(Team_select_popup_call_content, self).__init__(*args, **kwargs)
        self.caller = caller
        self.cnt = 0
        self.tgl_ids = ['tgl-' + str(i) for i in range(1, 11)]

    def clck(self, instance):
        if instance.state == 'down':
            try:
                instance.text = self.btn_text[self.cnt]
                self.cnt += 1
            except IndexError:
                pass
        else:
            instance.text = ""
            self.cnt -= 1

    def check_123(self):
        for i in range(5):
            if self.ids[self.tgl_ids[i]].text == '1':
                return [self.teams[0][0], self.teams[1][0]]
        return [self.teams[1][0], self.teams[0][0]]

    def Finish(self):
        self.arrange = self.check_123()
        # for items in self.children:
        rel_dict = {"tgl-1": "player_1_1", "tgl-2": "player_2_1", "tgl-3": "player_3_1", "tgl-4": "player_4_1",
                    "tgl-5": "player_5_1",
                    "tgl-6": "player_1_2", "tgl-7": "player_2_2", "tgl-8": "player_3_2", "tgl-9": "player_4_2",
                    "tgl-10": "player_5_2",
                    "tgl-11": "None_1", "tgl-12": "None_2"}
        s = list()
        for i in range(len(rel_dict)):
            ad = self.ids['tgl-' + str(i + 1)].text
            try:
                ad = int(ad)
            except ValueError:
                if ad == "R1":
                    ad = 3.5
                elif ad == "R2":
                    ad = 6.5
                else:
                    ad = 1000
            name = self.ids[rel_dict['tgl-' + str(i + 1)]].text
            if name == "No reserve":
                name = ""
                name_id = 0
            else:
                name_id = self.ids[rel_dict['tgl-' + str(i + 1)]].mc_id
            s.append((ad, (name, name_id)))
        s.sort()
        for i in range(8):
            self.arrange.append(s[i][1])
        self.caller.tp_btn.st = "Null"
        self.caller.done(self.arrange)

    def set_names(self, team1, team2):
        self.teams = [team1, team2]
        # print(self.teams)
        self.ids['team_1'].text = Make_persian(team1[0])
        self.ids['player_1_1'].text = Make_persian(team1[1][1])
        self.ids['player_1_1'].mc_id = team1[1][0]
        self.ids['player_2_1'].text = Make_persian(team1[2][1])
        self.ids['player_2_1'].mc_id = team1[2][0]
        self.ids['player_3_1'].text = Make_persian(team1[3][1])
        self.ids['player_3_1'].mc_id = team1[3][0]
        self.ids['player_4_1'].text = Make_persian(team1[4][1])
        self.ids['player_4_1'].mc_id = team1[4][0]
        self.ids['player_5_1'].text = Make_persian(team1[5][1])
        self.ids['player_5_1'].mc_id = team1[5][0]

        self.ids['team_2'].text = Make_persian(team2[0])
        self.ids['player_1_2'].text = Make_persian(team2[1][1])
        self.ids['player_1_2'].mc_id = team2[1][0]
        self.ids['player_2_2'].text = Make_persian(team2[2][1])
        self.ids['player_2_2'].mc_id = team2[2][0]
        self.ids['player_3_2'].text = Make_persian(team2[3][1])
        self.ids['player_3_2'].mc_id = team2[3][0]
        self.ids['player_4_2'].text = Make_persian(team2[4][1])
        self.ids['player_4_2'].mc_id = team2[4][0]
        self.ids['player_5_2'].text = Make_persian(team2[5][1])
        self.ids['player_5_2'].mc_id = team2[5][0]

    def cncl(self):
        self.caller.dismiss()


class Team_select_popup_res_content(BoxLayout):
    def __init__(self, caller, mod='e', *args, **kwargs):
        super(Team_select_popup_res_content, self).__init__(*args, **kwargs)
        self.mod =mod
        self.ok = False
        self.caller = caller
        self.cnt = 0
        self.tgl_ids = ['tgl-' + str(i) for i in range(1, 11)]
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.updater = SITE.uploader(self.caller.sql)

    def set_names(self, team1, team2):
        self.team1 = team1
        self.team2 = team2
        self.ids["team_123"].text = Make_persian(self.team1[0])
        self.ids["pl_1"].text = Make_persian(self.team1[1])
        self.ids["pl_2"].text = Make_persian(self.team1[2])
        self.ids["pl_3"].text = Make_persian(self.team1[3])
        self.ids["pl_r1"].text = Make_persian(self.team1[4])
        self.ids["team_456"].text = Make_persian(self.team2[0])
        self.ids["pl_4"].text = Make_persian(self.team2[1])
        self.ids["pl_5"].text = Make_persian(self.team2[2])
        self.ids["pl_6"].text = Make_persian(self.team2[3])
        self.ids["pl_r2"].text = Make_persian(self.team2[4])

        self.ids["L1"].text = Make_persian(self.team1[3])
        self.ids["L2"].text = Make_persian(self.team1[1])
        self.ids["L3"].text = Make_persian(self.team1[2])
        self.ids["L4"].text = Make_persian(self.team1[1])
        self.ids["L5"].text = Make_persian(self.team1[3])
        self.ids["L6"].text = Make_persian(self.team1[2])
        self.ids["L7"].text = Make_persian(self.team1[1])
        self.ids["L8"].text = Make_persian(self.team1[2])
        self.ids["L9"].text = Make_persian(self.team1[3])

        self.ids["R1"].text = Make_persian(self.team2[3])
        self.ids["R2"].text = Make_persian(self.team2[2])
        self.ids["R3"].text = Make_persian(self.team2[1])
        self.ids["R4"].text = Make_persian(self.team2[3])
        self.ids["R5"].text = Make_persian(self.team2[1])
        self.ids["R6"].text = Make_persian(self.team2[2])
        self.ids["R7"].text = Make_persian(self.team2[1])
        self.ids["R8"].text = Make_persian(self.team2[3])
        self.ids["R9"].text = Make_persian(self.team2[2])
        self.ok = True

        self.status = self.caller.sql.check_game_status(self.caller.game_id, mod=self.mod)
        if self.status == 'Finished':
            results = self.caller.sql.extract_game_result(self.caller.game_id, mod=self.mod)
            self.resss  = results
            self.ids["LS1"].text = results[2].split("-")[0]
            self.ids["LS2"].text = results[3].split("-")[0]
            self.ids["LS3"].text = results[4].split("-")[0]
            self.ids["LS4"].text = results[5].split("-")[0]
            self.ids["LS5"].text = results[6].split("-")[0]
            self.ids["LS6"].text = results[7].split("-")[0]
            self.ids["LS7"].text = results[8].split("-")[0]
            self.ids["LS8"].text = results[9].split("-")[0]
            self.ids["LS9"].text = results[10].split("-")[0]

            self.ids["RS1"].text = results[2].split("-")[1]
            self.ids["RS2"].text = results[3].split("-")[1]
            self.ids["RS3"].text = results[4].split("-")[1]
            self.ids["RS4"].text = results[5].split("-")[1]
            self.ids["RS5"].text = results[6].split("-")[1]
            self.ids["RS6"].text = results[7].split("-")[1]
            self.ids["RS7"].text = results[8].split("-")[1]
            self.ids["RS8"].text = results[9].split("-")[1]
            self.ids["RS9"].text = results[10].split("-")[1]

            for i in range(2,11):
                if results[i].split("-")[2] == 'l':
                    self.ids["LR_"+str(i-1)].state = 'down'
                elif results[i].split("-")[2] == 'r':
                    self.ids["RR_" + str(i - 1)].state = 'down'
                elif results[i].split("-")[2] == 'lr':
                    self.ids["RR_" + str(i - 1)].state = 'down'
                    self.ids["LR_" + str(i - 1)].state = 'down'
        return

    def close(self):
        if self.ok:
            team1 = self.team1
            team2 = self.team2
            team1 = [team1[0], (Make_persian(team1[1]), 0), (Make_persian(team1[2]), 0), (Make_persian(team1[3]), 0),
                     (Make_persian(team1[4]), 0)]
            team2 = [team2[0], (Make_persian(team2[1]), 0), (Make_persian(team2[2]), 0), (Make_persian(team2[3]), 0),
                     (Make_persian(team2[4]), 0)]

            if self.mod == "e":
                self.caller.caller.screenmanager.current_screen.ids.epee_paper.name_putter(team1, team2)
            elif self.mod == 'f':
                self.caller.caller.screenmanager.current_screen.ids.foil_paper.name_putter(team1, team2)
            elif self.mod == 's':
                self.caller.caller.screenmanager.current_screen.ids.sabre_paper.name_putter(team1, team2)

        self.caller.dismiss()

    def edit_arrange(self):
        self.caller.cnt = Team_select_popup_call_content(caller=self.caller)
        self.caller.content = self.caller.cnt
        Clock.schedule_once(self.caller.rearrange)
        pass

    def fill(self, side):
        if side == 'l':
            for i in range(1, 10):
                self.ids["LS" + str(i)].text = str(i * 5)
        else:
            for i in range(1, 10):
                self.ids["RS" + str(i)].text = str(i * 5)

    def finish(self):
        game_scores = [(int(self.ids["LS1"].text), int(self.ids["RS1"].text)),
                       (int(self.ids["LS2"].text), int(self.ids["RS2"].text)),
                       (int(self.ids["LS3"].text), int(self.ids["RS3"].text)),
                       (int(self.ids["LS4"].text), int(self.ids["RS4"].text)),
                       (int(self.ids["LS5"].text), int(self.ids["RS5"].text)),
                       (int(self.ids["LS6"].text), int(self.ids["RS6"].text)),
                       (int(self.ids["LS7"].text), int(self.ids["RS7"].text)),
                       (int(self.ids["LS8"].text), int(self.ids["RS8"].text)),
                       (int(self.ids["LS9"].text), int(self.ids["RS9"].text))]

        game_scores_res = game_scores.copy()

        g_s = [str(i[0]) + "-" + str(i[1]) + "-" for i in game_scores]

        g_id = self.caller.sql.extract_game_id(self.team1[0], self.team2[0], mod=self.caller.mod)

        for i in range(1, 10):
            if self.ids["LR_" + str(i)].state == "down":
                g_s[i - 1] += "l"
                game_scores_res[i - 1] = (game_scores_res[i - 1][0], game_scores_res[i - 1][1], 'l')
            if self.ids["RR_" + str(i)].state == "down":
                g_s[i - 1] += "r"
                if len(game_scores_res[i - 1]) == 2:
                    game_scores_res[i - 1] = (game_scores_res[i - 1][0], game_scores_res[i - 1][1], 'r')
                else:
                    game_scores_res[i - 1] = (game_scores_res[i - 1][0], game_scores_res[i - 1][1], 'lr')

        if game_scores[8][0] > game_scores[8][1]:
            winner, looser, score = self.team1[0], self.team2[0], (game_scores[8][0], game_scores[8][1])
        else:
            winner, looser, score = self.team2[0], self.team1[0], (game_scores[8][1], game_scores[8][0])

        if self.status == "Finished":
            self.caller.sql.update_game_res(g_id, g_s, mod=self.caller.mod)
            self.caller.caller.set_game_winner(winner, looser, score, mod=self.caller.mod)
            old_game = self.resss
            self.update_player_scores(game_scores_res, old_game)
        else:
            self.caller.sql.add_game_res(g_id, g_s, mod=self.caller.mod)
            self.caller.tp_btn.chng("Finished")
            self.caller.caller.set_game_winner(winner, looser, score, mod=self.caller.mod)
            self.add_player_scores(game_scores_res)

        self.caller.dismiss()
        self.updater.update_site()

    def add_player_scores(self, scores):

        # print(self.team1)
        # print(self.team2)
        game_ids = {1: (1, 3, 6), 2: (2, 5, 7), 3: (0, 4, 8), 4: (2, 4, 6), 5: (1, 5, 8), 6: (0, 3, 7)}

        # calculation for team 1
        t_1_difs = list()
        try:
            l_r_pl_id = self.caller.sql.Player_name2ID(self.team1[4])
        except:
            l_r_pl_id = 0
        r_dif = 0
        r_g_p = 0
        for i in range(1, 4):
            player = self.team1[i]
            pl_id = self.caller.sql.Player_name2ID(player)
            dif = 0
            g_p = 0
            for g in game_ids[i]:

                if (len(scores[g]) == 2) or (scores[g][2] == 'r'):
                    g_p += 1
                    if g >= 1:
                        dif += self.tafazol_1(scores[g], scores[g - 1], mod='l')
                    else:
                        dif += self.tafazol_1(scores[g], (0, 0), mod='l')

                elif (scores[g][2] == 'l') or (scores[g][2] == 'lr'):
                    r_g_p += 1
                    if g >= 1:
                        r_dif += self.tafazol_1(scores[g], scores[g - 1], mod='l')
                    else:
                        r_dif += self.tafazol_1(scores[g], (0, 0), mod='l')

            t_1_difs.append((pl_id, player, dif, g_p))

        t_1_difs.append((l_r_pl_id, self.team1[4], r_dif, r_g_p))

        # calculation for team 2
        t_2_difs = list()
        try:
            r_r_pl_id = self.caller.sql.Player_name2ID(self.team2[4])
        except:
            r_r_pl_id = 0
        r_dif = 0
        r_g_p = 0
        for i in range(4, 7):
            player = self.team2[i - 3]
            pl_id = self.caller.sql.Player_name2ID(player)
            dif = 0
            g_p = 0
            for g in game_ids[i]:

                if (len(scores[g]) == 2) or (scores[g][2] == 'l'):
                    g_p += 1
                    if g >= 1:
                        dif += self.tafazol_1(scores[g], scores[g - 1], mod='r')
                    else:
                        dif += self.tafazol_1(scores[g], (0, 0), mod='r')

                elif (scores[g][2] == 'r') or (scores[g][2] == 'lr'):
                    r_g_p += 1
                    if g >= 1:
                        r_dif += self.tafazol_1(scores[g], scores[g - 1], mod='r')
                    else:
                        r_dif += self.tafazol_1(scores[g], (0, 0), mod='r')

            t_2_difs.append((pl_id, player, dif, g_p))

        t_2_difs.append((r_r_pl_id, self.team2[4], r_dif, r_g_p))
        for pl in t_1_difs:
            try:
                self.player_updater(pl)
            except ValueError:
                pass

        for pl in t_2_difs:
            try:
                self.player_updater(pl)
            except ValueError:
                pass
        return

    def update_player_scores(self, scores, old_game):
        game_ids = {1: (1, 3, 6), 2: (2, 5, 7), 3: (0, 4, 8), 4: (2, 4, 6), 5: (1, 5, 8), 6: (0, 3, 7)}

        # calculation for team 1
        t_1_difs = list()
        try:
            l_r_pl_id = self.caller.sql.Player_name2ID(self.team1[4])
        except:
            l_r_pl_id = 0
        r_dif = 0
        r_g_p = 0
        o_r_dif = 0

        for i in range(1, 4):
            player = self.team1[i]
            pl_id = self.caller.sql.Player_name2ID(player)
            o_dif = 0


            dif = 0
            g_p = 0

            for g in game_ids[i]:

                if (len(scores[g]) == 2) or (scores[g][2] == 'r'):
                    g_p += 1
                    if g >= 1:
                        dif += self.tafazol_1(scores[g], scores[g - 1], mod='l')
                        os = (old_game[g+2].split("-")[0],old_game[g+2].split("-")[1])
                        os1 = (old_game[g+1].split("-")[0],old_game[g+1].split("-")[1])
                        o_dif += self.tafazol_1(os, os1, mod='l')
                    else:
                        dif += self.tafazol_1(scores[g], (0, 0), mod='l')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        o_dif += self.tafazol_1(os, (0,0), mod='l')

                elif (scores[g][2] == 'l') or (scores[g][2] == 'lr'):
                    r_g_p += 1
                    if g >= 1:
                        r_dif += self.tafazol_1(scores[g], scores[g - 1], mod='l')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        os1 = (old_game[g + 1].split("-")[0], old_game[g + 1].split("-")[1])
                        o_r_dif += self.tafazol_1(os, os1, mod='l')
                    else:
                        r_dif += self.tafazol_1(scores[g], (0, 0), mod='l')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        o_r_dif += self.tafazol_1(os, (0,0), mod='l')

            t_1_difs.append((pl_id, player, dif-o_dif, 0))

        t_1_difs.append((l_r_pl_id, self.team1[4], r_dif-o_r_dif, 0))

        # calculation for team 2
        t_2_difs = list()
        try:
            r_r_pl_id = self.caller.sql.Player_name2ID(self.team2[4])
        except:
            r_r_pl_id = 0
        r_dif = 0
        r_g_p = 0
        o_r_dif = 0
        for i in range(4, 7):
            player = self.team2[i - 3]
            pl_id = self.caller.sql.Player_name2ID(player)
            dif = 0
            o_dif = 0
            g_p = 0
            for g in game_ids[i]:

                if (len(scores[g]) == 2) or (scores[g][2] == 'l'):
                    g_p += 1
                    if g >= 1:
                        dif += self.tafazol_1(scores[g], scores[g - 1], mod='r')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        os1 = (old_game[g + 1].split("-")[0], old_game[g + 1].split("-")[1])
                        o_dif += self.tafazol_1(os, os1, mod='r')
                    else:
                        dif += self.tafazol_1(scores[g], (0, 0), mod='r')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        o_dif += self.tafazol_1(os, (0,0), mod='r')

                elif (scores[g][2] == 'r') or (scores[g][2] == 'lr'):
                    r_g_p += 1
                    if g >= 1:
                        r_dif += self.tafazol_1(scores[g], scores[g - 1], mod='r')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        os1 = (old_game[g + 1].split("-")[0], old_game[g + 1].split("-")[1])
                        o_r_dif += self.tafazol_1(os, os1, mod='r')
                    else:
                        r_dif += self.tafazol_1(scores[g], (0, 0), mod='r')
                        os = (old_game[g + 2].split("-")[0], old_game[g + 2].split("-")[1])
                        o_r_dif += self.tafazol_1(os, (0,0), mod='r')

            t_2_difs.append((pl_id, player, dif-o_dif, 0))

        t_2_difs.append((r_r_pl_id, self.team2[4], r_dif-o_r_dif, 0))

        for pl in t_1_difs:
            try:
                self.player_updater(pl)
            except ValueError:
                pass

        for pl in t_2_difs:
            try:
                self.player_updater(pl)
            except ValueError:
                pass
        return
        pass

    def tafazol_1(self, ngame, lgame, mod='r'):
        if mod == 'r':
            dif = (int(ngame[1]) - int(lgame[1])) - (int(ngame[0]) - int(lgame[0]))
        else:
            dif = (int(ngame[0]) - int(lgame[0])) - (int(ngame[1]) - int(lgame[1]))
        return dif

    def player_updater(self,data):
        Id = data[0]
        name = data[1]
        dif = data[2]
        g_p = data[3]
        self.caller.sql.Player_add_sc(Id,dif,g_p)


class Team_select_popup(Popup):
    def __init__(self, caller, btn, game_name, mod='e', *args, **kwargs):
        super(Team_select_popup, self).__init__(*args, **kwargs)
        self.tp_btn = btn
        self.mod = mod
        if self.tp_btn.st == "Null":
            self.stat = "call"
        elif self.tp_btn.st == "Running":
            self.stat = "res"
        else:
            self.stat = "fin"

        if self.stat == "call":
            self.cnt = Team_select_popup_call_content(caller=self)

        elif self.stat == "res":
            self.cnt = Team_select_popup_res_content(caller=self, mod=self.mod)

        elif self.stat == "fin":
            self.cnt = Team_select_popup_res_content(caller=self, mod=self.mod)

        self.content = self.cnt

        self.game_id = game_name
        self.caller = caller
        self.sql = self.caller.sql
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        if self.stat == "call":
            res = self.sql.extract_team_player(name=Make_persian(self.game_id[0]), mode=self.mod)

            for i in range(5 - len(res)):
                res.append((0, ''))

            t1 = [Make_persian(self.game_id[0]), res[0], res[1], res[2], res[3], res[4]]

            res = self.sql.extract_team_player(name=Make_persian(self.game_id[1]), mode=self.mod)
            for i in range(5 - len(res)):
                res.append((0, ''))
            t2 = [Make_persian(self.game_id[1]), res[0], res[1], res[2], res[3], res[4]]
            self.content.set_names(t1, t2)

        elif self.stat == "res":
            game_ID = self.sql.extract_game_id(Make_persian(self.game_id[0]),
                                               Make_persian(self.game_id[1]), mod=self.mod)
            res = self.sql.extract_games(mod=self.mod, ID=game_ID)
            t1 = res[0][0:6]
            t2 = res[0][6:]
            p1 = self.sql.Player_ID2name(t1[2])
            p2 = self.sql.Player_ID2name(t1[3])
            p3 = self.sql.Player_ID2name(t1[4])
            r1 = self.sql.Player_ID2name(t1[5])
            p4 = self.sql.Player_ID2name(t2[2])
            p5 = self.sql.Player_ID2name(t2[3])
            p6 = self.sql.Player_ID2name(t2[4])
            r2 = self.sql.Player_ID2name(t2[5])

            t1 = [t1[1], p1, p2, p3, r1]
            t2 = [t2[1], p4, p5, p6, r2]

            self.content.set_names(t1, t2)
        elif self.stat == "fin":
            game_ID = self.sql.extract_game_id(Make_persian(self.game_id[0]),
                                               Make_persian(self.game_id[1]), mod=self.mod)
            res = self.sql.extract_games(mod=self.mod, ID=game_ID)
            t1 = res[0][0:6]
            t2 = res[0][6:]
            p1 = self.sql.Player_ID2name(t1[2])
            p2 = self.sql.Player_ID2name(t1[3])
            p3 = self.sql.Player_ID2name(t1[4])
            r1 = self.sql.Player_ID2name(t1[5])
            p4 = self.sql.Player_ID2name(t2[2])
            p5 = self.sql.Player_ID2name(t2[3])
            p6 = self.sql.Player_ID2name(t2[4])
            r2 = self.sql.Player_ID2name(t2[5])

            t1 = [t1[1], p1, p2, p3, r1]
            t2 = [t2[1], p4, p5, p6, r2]

            self.content.set_names(t1, t2)


    def rearrange(self, dt):
        res = self.sql.extract_team_player(name=Make_persian(self.game_id[0]), mode=self.mod)

        for i in range(5 - len(res)):
            res.append((0, ''))

        t1 = [Make_persian(self.game_id[0]), res[0], res[1], res[2], res[3], res[4]]

        res = self.sql.extract_team_player(name=Make_persian(self.game_id[1]), mode=self.mod)
        for i in range(5 - len(res)):
            res.append((0, ''))
        t2 = [Make_persian(self.game_id[1]), res[0], res[1], res[2], res[3], res[4]]
        self.content.set_names(t1, t2)

        pass

    def done(self, names):
        if self.tp_btn.st == "Null":
            team_1 = [names[0]] + names[2:6]
            team_2 = [names[1]] + names[6:]
            g_i = self.sql.extract_game_id(Make_persian(self.tp_btn.game_id[0]),
                                           Make_persian(self.tp_btn.game_id[1]), mod=self.mod)
            self.sql.update_game_plays(g_i, team_1, team_2, mod=self.mod)
            self.tp_btn.chng("Running")

        self.caller.screenmanager.current_screen.name_putter(team_1, team_2, self.mod)
        self.dismiss()
