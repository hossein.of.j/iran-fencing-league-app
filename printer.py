from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
import reportlab.fonts as Fonts
from bidi.algorithm import get_display
import arabic_reshaper
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
pdfmetrics.registerFont(TTFont('B Nazanin', 'B Nazanin.ttf'))
import datetime
import win32print


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


def Team_sheet_pdf_make(team1, team2, cp, mod='e'):
    poss = {"team1": (80, 532), "P_1": (80, 505), "P_2": (80, 475), "P_3": (80, 445), "R_1": (80, 415),
            "team2": (410, 532), "P_4": (430, 505), "P_5": (430, 475), "P_6": (430, 445), "R_2": (430, 415)}

    strs = {"team1": team1[0], "P_1": team1[1], "P_2": team1[2], "P_3": team1[3],
            "R_1": team1[4],
            "team2": team2[0], "P_4": team2[1], "P_5": team2[2], "P_6": team2[3],
            "R_2": team2[4]}

    items = ["team1", "P_1", "P_2", "P_3", "R_1",
             "team2", "P_4", "P_5", "P_6", "R_2"]

    tbl = [(80, 792-460 , strs["P_3"]), (80, 792-488, strs["P_1"]), (80, 792-516, strs["P_2"]), (80, 792-544, strs["P_1"]),
           (80, 792-572, strs["P_3"]), (80, 792-600, strs["P_2"]), (80, 792-628, strs["P_1"]), (80, 792-656, strs["P_2"]),
           (80, 792-684, strs["P_3"]),
           (430, 792-460, strs["P_6"]), (430, 792-488, strs["P_5"]), (430, 792-516, strs["P_4"]), (430, 792-544, strs["P_6"]),
           (430, 792-572, strs["P_4"]), (430, 792-600, strs["P_5"]), (430, 792-628, strs["P_4"]), (430, 792-656, strs["P_6"]),
           (430, 792-684, strs["P_5"])]

    packet = io.BytesIO()
    # create a new PDF with Reportlab

    can = canvas.Canvas(packet, pagesize=A4)
    # can.rotate(90)

    can.setFont("B Nazanin", 18)
    for itm in items:
        can.drawString(poss[itm][0], poss[itm][1], Make_persian(strs[itm]))
    #
    for it in tbl:
        can.drawString(it[0], it[1], Make_persian(it[2]))

    can.save()

    #move to the beginning of the StringIO buffer

    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF
    if mod == 'e':
        existing_pdf = PdfFileReader(open("Sample_sheet_e.pdf", "rb"))
    elif mod == 'f':
        existing_pdf = PdfFileReader(open("Sample_sheet_f.pdf", "rb"))
    elif mod == 's':
        existing_pdf = PdfFileReader(open("Sample_sheet_s.pdf", "rb"))
    else:
        existing_pdf = PdfFileReader(open("Sample_sheet.pdf", "rb"))
    output = PdfFileWriter()
    # add the "watermark" (which is the new pdf) on the existing page

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    # f_name = str(datetime.datetime.no)
    outputStream = open("Temp.pdf", "wb")
    output.write(outputStream)
    outputStream.close()

    setcopies(cp)


def Team_form_pdf_make(weapon, team, pls):
    poss = [(210,460),(350,460),(50,355),(50,333),(50,310),(50,285),(50,262)]

    packet = io.BytesIO()
    # create a new PDF with Reportlab

    can = canvas.Canvas(packet, pagesize=A4)
    #can.rotate(90)

    can.setFont("B Nazanin", 30)

    can.drawString(poss[0][0], poss[0][1], weapon)
    can.drawString(poss[1][0], poss[1][1], team)

    can.setFont("B Nazanin", 18)
    for i in range(len(pls)):
        can.drawString(poss[2+i][0], poss[2+i][1], Make_persian(pls[i]))

    can.save()

    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF

    existing_pdf = PdfFileReader(open("team form.pdf", "rb"))
    output = PdfFileWriter()
    # add the "watermark" (which is the new pdf) on the existing page

    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    # f_name = str(datetime.datetime.no)
    outputStream = open("Temp.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    setcopies(1)


def send_to_printer():
    import win32api
    set_to_A4()
    fname = "Temp.pdf"
    win32api.ShellExecute(0, 'print', fname, None, '.', 0)


def send_to_printer_l():
    import win32api
    set_to_A5()
    fname = "l_temp.pdf"
    win32api.ShellExecute(0, 'print', fname, None, '.', 0)


def setcopies(i):
    import win32print
    PRINTER_DEFAULTS = {"DesiredAccess": win32print.PRINTER_ALL_ACCESS}
    p = win32print.GetDefaultPrinter()
    ph = win32print.OpenPrinter(p,PRINTER_DEFAULTS)
    pro = win32print.GetPrinter(ph,2)
    pro["pDevMode"].Copies = i
    win32print.SetPrinter(ph,2,pro,0)


def resize_to_A5():
    pdf = open("Temp.pdf", 'rb')
    p = PdfFileReader(pdf)
    p_0 = p.getPage(0)
    p_0.scale(148. / 210., 210. / 297.)
    out_pdf = open("l_temp.pdf", 'wb')
    pw = PdfFileWriter()
    pw.addPage(p_0)

    pw.write(out_pdf)


def set_to_A5():
    resize_to_A5()
    import win32print
    import win32con
    PRINTER_DEFAULTS = {"DesiredAccess": win32print.PRINTER_ALL_ACCESS}
    p = win32print.GetDefaultPrinter()
    ph = win32print.OpenPrinter(p, PRINTER_DEFAULTS)
    pro = win32print.GetPrinter(ph, 2)
    pro["pDevMode"].PaperSize = win32con.DMPAPER_A5
    win32print.SetPrinter(ph, 2, pro, 0)


def set_to_A4():
    import win32print
    import win32con
    PRINTER_DEFAULTS = {"DesiredAccess": win32print.PRINTER_ALL_ACCESS}
    p = win32print.GetDefaultPrinter()
    ph = win32print.OpenPrinter(p, PRINTER_DEFAULTS)
    pro = win32print.GetPrinter(ph, 2)
    pro["pDevMode"].PaperSize = win32con.DMPAPER_A4
    win32print.SetPrinter(ph, 2, pro, 0)


if __name__ == "__main__":
    # team1 = ["تیم اول وارش",'حسین جعفری','محمدعلی کیهانی','محمدرضایی','محمد اسماعیلی']
    # team2 = ["تیم دوم اردبیل",'طاهر عاشوری','امیررضا کنعانی','احمدرضا کنعانی','وثوقی']
    # Team_sheet_pdf_make(team1,team2)
    # Team_form_pdf_make("اپه","بیکاران هیئت یزد",["حسین جعفری","محمد علی کیهانی","محمد رضایی طادی","مجید بلند نظر","محمد اسماعیلی"])
    # send_to_printer()
    set_to_A4()
    # send_to_printer()
