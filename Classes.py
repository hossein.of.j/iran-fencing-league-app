class Player:
    name = ""
    gender = ""
    age = ""
    rh = True

    def __init__(self, name="", gender="Male", age="14", rh=True):
        self.name = name
        self.gender = gender
        self.age = age
        self.rh = rh


class Team:
    name = ""

    def __init__(self, name=""):
        self.name = name

