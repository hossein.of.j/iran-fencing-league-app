from extra_classes import *


class Teams_screen(Screen):
    def __init__(self, *args, **kwargs):
        super(Teams_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)
        self.e_p_ids = [None,None,None,None,None]
        self.f_p_ids = [None,None,None,None,None]
        self.s_p_ids = [None,None,None,None,None]

    def _finish_init(self, dt):
        self.sql = self.manager.sql
        self.teamlist = self.ids.teams_list
        self.Teams_list_for_add()

    def refresh(self):
        self.teamlist.clear_widgets()
        self.teamlist.add_widget(Team_show_item(name="نام", e="E", f="F", s="S"))
        Clock.schedule_once(self._finish_init)

    def Teams_list_for_add_update(self):
        self.teamlist.clear_widgets()
        teams = self.sql.extract_all_team()
        self.teamlist.add_widget(Team_show_item(name="نام", e="E", f="F", s="S"))
        self.teams = list()
        for team in teams:
            wid = Team_show_item()
            wid.caller = self
            wid.name = team[1]
            if team[2]:
                wid.e = "*"
            if team[7]:
                wid.f = "*"
            if team[12]:
                wid.s = "*"
            self.teamlist.add_widget(wid)
            self.teams.append(team[1])

    def Teams_list_for_add(self):
        teams = self.sql.extract_all_team()
        self.teams= list()
        for team in teams:
            wid = Team_show_item()
            wid.caller = self
            wid.name = team[1]
            if team[2]:
                wid.e = "*"
            if team[7]:
                wid.f = "*"
            if team[12]:
                wid.s = "*"
            self.teamlist.add_widget(wid)
            self.teams.append(team[1])

    def add_team(self):

        team_name = Make_persian(self.ids.team_name.text)
        if team_name in self.teams:
            e_p = list()
            f_p = list()
            s_p = list()
            for i in range(5):
                e_p.append(self.ids["e_p_"+str(i+1)].text)
                f_p.append(self.ids["f_p_"+str(i+1)].text)
                s_p.append(self.ids["s_p_"+str(i+1)].text)

                if self.e_p_ids[i]:
                    self.sql.update_player_name(name=Make_persian(e_p[i]), ID=self.e_p_ids[i])
                else:
                    n_p = Player(name=Make_persian(e_p[i]))
                    n_id = self.sql.add_player(name=n_p.name)
                    pos= self.e_p_ids.index(None)
                    self.e_p_ids[pos]=n_id[0][0]
                    self.sql.update_team_players(team_name,self.e_p_ids, mod='e')

            if self.f_p_ids[i]:
                self.sql.update_player_name(name=Make_persian(f_p[i]), ID=self.f_p_ids[i])
            else:
                n_p = Player(name=Make_persian(f_p[i]))
                n_id = self.sql.add_player(name=n_p.name)
                pos = self.f_p_ids.index(None)
                self.f_p_ids[pos] = n_id[0][0]
                self.sql.update_team_players(team_name, self.f_p_ids, mod='f')

            if self.s_p_ids[i]:
                self.sql.update_player_name(name=Make_persian(s_p[i]), ID=self.s_p_ids[i])
            else:
                n_p = Player(name=Make_persian(s_p[i]))
                n_id = self.sql.add_player(name=n_p.name)
                pos = self.s_p_ids.index(None)
                self.s_p_ids[pos] = n_id[0][0]
                self.sql.update_team_players(team_name, self.s_p_ids, mod='s')

            return
        e_p = list()
        for i in range(5):
            e_p.append(Player(name=Make_persian(self.ids['e_p_' + str(i + 1)].text)))
            if e_p[-1].name == "": del e_p[-1]
        f_p = list()
        for i in range(5):
            f_p.append(Player(name=Make_persian(self.ids['f_p_' + str(i + 1)].text)))
            if f_p[-1].name == "": del f_p[-1]
        s_p = list()
        for i in range(5):
            s_p.append(Player(name=Make_persian(self.ids['s_p_' + str(i + 1)].text)))
            if s_p[-1].name == "": del s_p[-1]

        self.sql.add_team(team_name, e_p, f_p, s_p)
        self.Teams_list_for_add_update()
        ids = {'team_name', 'e_p_1', 'e_p_2', 'e_p_3', 'e_p_4', 'e_p_5',
               'f_p_1', 'f_p_2', 'f_p_3', 'f_p_4', 'f_p_5',
               's_p_1', 's_p_2', 's_p_3', 's_p_4', 's_p_5'}

        for child in ids:
            self.ids[child].del_all()
        self.e_p_ids = [None, None, None, None, None]
        self.f_p_ids = [None, None, None, None, None]
        self.s_p_ids = [None, None, None, None, None]

    def put_team(self, t_name):
        team_id = self.sql.Team_name2ID(t_name)
        self.ids["team_name"].text = Make_persian(t_name)
        e_pls = self.sql.extract_team_player(ID=team_id, mode='e')
        f_pls = self.sql.extract_team_player(ID=team_id, mode='f')
        s_pls = self.sql.extract_team_player(ID=team_id, mode='s')

        ids = {'team_name', 'e_p_1', 'e_p_2', 'e_p_3', 'e_p_4', 'e_p_5',
               'f_p_1', 'f_p_2', 'f_p_3', 'f_p_4', 'f_p_5',
               's_p_1', 's_p_2', 's_p_3', 's_p_4', 's_p_5'}

        for child in ids:
            self.ids[child].del_all()

        self.ids["team_name"].text = Make_persian(t_name)

        for e in range(len(e_pls)):
            self.ids["e_p_"+str(e+1)].text = Make_persian(e_pls[e][1])
            self.e_p_ids[e] = e_pls[e][0]

        for f in range(len(f_pls)):
            self.ids["f_p_"+str(f+1)].text = Make_persian(f_pls[f][1])
            self.f_p_ids[f] = f_pls[f][0]

        for s in range(len(s_pls)):
            self.ids["s_p_"+str(s+1)].text = Make_persian(s_pls[s][1])
            self.s_p_ids[s] = s_pls[s][0]

        return


    def xlsx_all_players(self):
        pass