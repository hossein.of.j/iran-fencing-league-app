from extra_classes import *


class TeamNameBtn(ToggleButton):
    weapon = StringProperty()
    team_id= NumericProperty()

    def __init__(self, weapon, team_id, *args, **kwargs):
        super(TeamNameBtn, self).__init__(*args, **kwargs)
        self.weapon = weapon
        self.team_id = team_id


class Team_forms_screen(Screen):
    def __init__(self, *args, **kwargs):
        super(Team_forms_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)
        # self.sql = db.Databasecontroller()

    def _finish_init(self, *dt):
        self.sql = self.manager.sql
        pass

    def setter(self, btn):
        if btn.state == "down":
            if btn.weapon == "e":
                weap = Make_persian("اپه")
                pls = self.sql.extract_team_player(ID=btn.team_id, mode="e")
            elif btn.weapon == "f":
                weap = Make_persian("فلوره")
                pls = self.sql.extract_team_player(ID=btn.team_id, mode="f")
            else:
                weap = Make_persian("سابر")
                pls = self.sql.extract_team_player(ID=btn.team_id, mode="s")

            team_name = btn.text

            if len(pls) < 5:
                pls = pls + [(0,"")] * (5-len(pls))
            self.ids.sheet.name_putter(weap, team_name, Make_persian(pls[0][1]), Make_persian(pls[1][1]),
                                       Make_persian(pls[2][1]), Make_persian(pls[3][1]), Make_persian(pls[4][1]))
            self.visible = [weap,team_name,pls]

    def on_enter(self, *args):
        e_teams = self.team_extarctor('epee')
        f_teams = self.team_extarctor('foil')
        s_teams = self.team_extarctor('sabre')

        self.etbtn = list()
        self.ftbtn = list()
        self.stbtn = list()

        self.ids.Epee_team_list.clear_widgets()
        self.ids.Foil_team_list.clear_widgets()
        self.ids.Sabre_team_list.clear_widgets()

        for e_t in e_teams:
            self.etbtn.append(TeamNameBtn(text=Make_persian(e_t[2]), weapon="e", team_id=e_t[1]))
            self.etbtn[-1].bind(on_press=self.setter)
            self.ids.Epee_team_list.add_widget(self.etbtn[-1])

        for f_t in f_teams:
            self.ftbtn.append(TeamNameBtn(text=Make_persian(f_t[2]), weapon="f", team_id=f_t[1]))
            self.ftbtn[-1].bind(on_press=self.setter)
            self.ids.Foil_team_list.add_widget(self.ftbtn[-1])

        for s_t in s_teams:
            self.stbtn.append(TeamNameBtn(text=Make_persian(s_t[2]), weapon="s", team_id=s_t[1]))
            self.stbtn[-1].bind(on_press=self.setter)
            self.ids.Sabre_team_list.add_widget(self.stbtn[-1])
        return

    def team_extarctor(self, mod):
        res = self.sql.extract_all_team(weapon=mod)
        teams = list()

        if mod == 'epee':
            for i in res:
                teams.append(('e',i[0],i[1]))
        elif mod == 'foil':
            for i in res:
                teams.append(('f',i[0],i[1]))
        elif mod == 'sabre':
            for i in res:
                teams.append(('s',i[0],i[1]))
        return teams

    def printer(self, item):
        pls = [i[1] for i in item[2]]
        PRINT.Team_form_pdf_make(item[0], item[1], pls)
        PRINT.send_to_printer()

    def print_one(self):
        try:
            self.printer(self.visible)
        except AttributeError:
            pass

    def print_all(self):
        e_teams = self.team_extarctor('epee')
        f_teams = self.team_extarctor('foil')
        s_teams = self.team_extarctor('sabre')

        e_list=list()
        f_list=list()
        s_list=list()

        for e in e_teams:
            weap = Make_persian("اپه")
            team_name = Make_persian(e[2])
            pls = self.sql.extract_team_player(ID=e[1], mode="e")
            if len(pls) < 5:
                pls = pls + [(0,"")] * (5-len(pls))
            e_list.append((weap,team_name,pls))

        for e in f_teams:
            weap = Make_persian("فلوره")
            team_name = Make_persian(e[2])
            pls = self.sql.extract_team_player(ID=e[1], mode="f")
            if len(pls) < 5:
                pls = pls + [(0,"")] * (5-len(pls))
            f_list.append((weap,team_name,pls))

        for e in s_teams:
            weap = Make_persian("سابر")
            team_name = Make_persian(e[2])
            pls = self.sql.extract_team_player(ID=e[1], mode="s")
            if len(pls) < 5:
                pls = pls + [(0,"")] * (5-len(pls))
            s_list.append((weap,team_name,pls))

        kat = e_list + f_list + s_list

        for team in kat:
            self.printer(team)

    def refresh(self):
        self._finish_init()

