from extra_classes import *
import uploader as SITE

class League_screen(Screen):

    def __init__(self, *args, **kwargs):
        super(League_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)
        self.games_btn = list()
        self.f_games_btn = list()
        self.s_games_btn = list()

    def _finish_init(self, dt):
        self.sql = self.manager.sql
        self.updater = SITE.uploader(self.sql)

        self.e_l, self.f_l, self.s_l = self.sql.check_exiting_leaque()

        if self.e_l:
            self.ids['dis_epee'].opacity = 0
            self.ids['dis_epee'].pos = (0,0)
            self.ids['dis_epee'].size = (0,0)
            self.build_table("e")
        else:
            self.ids['dis_epee'].opacity = 1
            self.ids['dis_epee'].pos = (0,0)
            self.ids['dis_epee'].size = self.size

        if self.f_l:
            self.ids['dis_foil'].opacity = 0
            self.ids['dis_foil'].pos = (0, 0)
            self.ids['dis_foil'].size = (0, 0)
            self.build_table("f")
        else:
            self.ids['dis_foil'].opacity = 1
            self.ids['dis_foil'].pos = (0,0)
            self.ids['dis_foil'].size = self.size

        if self.s_l:
            self.ids['dis_sabre'].opacity = 0
            self.ids['dis_sabre'].pos = (0, 0)
            self.ids['dis_sabre'].size = (0, 0)
            self.build_table("s")
        else:
            self.ids['dis_sabre'].opacity = 1
            self.ids['dis_sabre'].pos = (0, 0)
            self.ids['dis_sabre'].size = self.size

    def build_table(self, w):
        if w == "e":
            epee_top = self.ids.epee_table_top
            epee_top.clear_widgets()
            epee_top.orientation = "vertical"
            teams = self.sql.extract_league_teams_ID()
            games = self.sql.extract_games(mod='e')
            self.team_bars = list()

            for items in teams:
                team_bar = Team_table_item(no=str(items[0]), name=Make_persian(items[1]), ttl=len(teams))
                self.team_bars.append(team_bar)
                epee_top.add_widget(self.team_bars[-1])

            for gm in games:
                state = gm[0]
                t_t = (self.sql.extract_team_pos(gm[1], mod='e'), Make_persian(gm[3]))
                b_t = (self.sql.extract_team_pos(gm[2], mod='e'), Make_persian(gm[4]))

                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.games_btn.append(
                    Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), status=state, caller=self, mod='e'))
                if state == "Finished":
                    g_id = self.sql.extract_game_id(Make_persian(t_t[1]), Make_persian(b_t[1]))
                    winner, looser, score = self.sql.extract_winner(g_id)
                    win_poss = self.sql.extract_team_pos(winner, 'e')
                    loos_poss = self.sql.extract_team_pos(looser, 'e')
                    self.team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
                    self.team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])

            self.ids.games_bar.clear_widgets()

            for btn in self.games_btn:
                self.ids.games_bar.add_widget(btn)

        elif w == "f":
            foil_top = self.ids.foil_table_top
            foil_top.clear_widgets()
            foil_top.orientation = "vertical"
            teams = self.sql.extract_league_teams_ID(mod='f')
            games = self.sql.extract_games(mod='f')
            self.f_team_bars = list()

            for items in teams:
                team_bar = Team_table_item(no=str(items[0]), name=Make_persian(items[1]), ttl=len(teams))
                self.f_team_bars.append(team_bar)
                foil_top.add_widget(self.f_team_bars[-1])

            for gm in games:
                state = gm[0]
                t_t = (self.sql.extract_team_pos(gm[1], mod='f'), Make_persian(gm[3]))
                b_t = (self.sql.extract_team_pos(gm[2], mod='f'), Make_persian(gm[4]))

                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.f_games_btn.append(
                    Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), status=state, caller=self, mod="f"))
                if state == "Finished":
                    g_id = self.sql.extract_game_id(Make_persian(t_t[1]), Make_persian(b_t[1]),mod='f')
                    winner, looser, score = self.sql.extract_winner(g_id, mod='f')
                    win_poss = self.sql.extract_team_pos(winner, 'f')
                    loos_poss = self.sql.extract_team_pos(looser, 'f')
                    self.f_team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
                    self.f_team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])

            self.ids.f_games_bar.clear_widgets()

            for btn in self.f_games_btn:
                self.ids.f_games_bar.add_widget(btn)

        elif w == "s":
            sabre_top = self.ids.sabre_table_top
            sabre_top.clear_widgets()
            sabre_top.orientation = "vertical"
            teams = self.sql.extract_league_teams_ID(mod='s')
            games = self.sql.extract_games(mod='s')
            self.s_team_bars = list()

            for items in teams:
                team_bar = Team_table_item(no=str(items[0]), name=Make_persian(items[1]), ttl=len(teams))
                self.s_team_bars.append(team_bar)
                sabre_top.add_widget(self.s_team_bars[-1])

            for gm in games:
                state = gm[0]
                t_t = (self.sql.extract_team_pos(gm[1], mod='s'), Make_persian(gm[3]))
                b_t = (self.sql.extract_team_pos(gm[2], mod='s'), Make_persian(gm[4]))

                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.s_games_btn.append(
                    Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), status=state, caller=self, mod="s"))
                if state == "Finished":
                    g_id = self.sql.extract_game_id(Make_persian(t_t[1]), Make_persian(b_t[1]),mod='s')
                    winner, looser, score = self.sql.extract_winner(g_id, mod='s')
                    win_poss = self.sql.extract_team_pos(winner, 's')
                    loos_poss = self.sql.extract_team_pos(looser, 's')
                    self.s_team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
                    self.s_team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])

            self.ids.s_games_bar.clear_widgets()

            for btn in self.s_games_btn:
                self.ids.s_games_bar.add_widget(btn)

    def set_game_winner(self, winner, looser, score, mod='e'):
        if mod == 'e':
            win_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(winner), 'e')
            loos_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(looser), 'e')
            self.team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
            self.team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])
        elif mod == 'f':
            win_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(winner), 'f')
            loos_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(looser), 'f')
            self.f_team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
            self.f_team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])
        elif mod == 's':
            win_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(winner), 's')
            loos_poss = self.sql.extract_team_pos(self.sql.Team_name2ID(looser), 's')
            self.s_team_bars[win_poss - 1].set_score(loos_poss - 1, "v", score[0])
            self.s_team_bars[loos_poss - 1].set_score(win_poss - 1, "l", score[1])
        # self.site_updater()

    def site_updater(self):
        self.updater.update_site()

    def refresh(self):
        self.e_l = False
        Clock.schedule_once(self._finish_init)
        self.games_btn = list()
        self.f_games_btn = list()
        self.s_games_btn = list()
        pass

    def epee_league_arrange(self, instance):
        if self.e_l:
            return
        teams = self.sql.extract_all_team('epee')
        if len(teams) < 6:
            Popup(title="Team arrangement", content=Label_f(text="تعداد تیم کافی نیست"), size_hint=(0.6, .6),
                                auto_dismiss=1).open()
            return
        self.mod = "e"
        self.pop = Team_pop(title="Team arrangement", content=Team_select_pop(teams), size_hint=(0.6, .9), caller=self,
                            auto_dismiss=0)
        self.pop.open()
        instance.parent.remove_widget(instance)

    def foil_league_arrange(self, instance):
        if self.f_l:
            return
        teams = self.sql.extract_all_team('foil')
        if len(teams) < 5:
            Popup(title="Team arrangement", content=Label_f(text="تعداد تیم کافی نیست"), size_hint=(0.6, .6),
                                auto_dismiss=1).open()
            return
        self.mod = "f"
        self.pop = Team_pop(title="Team arrangement", content=Team_select_pop(teams), size_hint=(0.6, .9), caller=self,
                            auto_dismiss=0)
        self.pop.open()
        instance.parent.remove_widget(instance)

    def sabre_league_arrange(self, instance):
        if self.s_l:
            return
        teams = self.sql.extract_all_team('sabre')
        if len(teams) < 5:
            Popup(title="Team arrangement", content=Label_f(text="تعداد تیم کافی نیست"), size_hint=(0.6, .6),
                                auto_dismiss=1).open()
            return
        self.mod = "s"
        self.pop = Team_pop(title="Team arrangement", content=Team_select_pop(teams), size_hint=(0.6, .9), caller=self,
                            auto_dismiss=0)
        self.pop.open()
        instance.parent.remove_widget(instance)

    def rearrenge(self, teams):
        if self.mod == "e":
            epee_top = self.ids.epee_table_top
            epee_top.orientation = "vertical"
            teams.sort()

            tt = list()
            for i in teams:
                tt.append(self.sql.team_name2ID(Make_persian(i[1])))

            self.sql.create_league('e', tt)

            for items in teams:
                epee_top.add_widget(Team_table_item(no=str(items[0]), name=items[1], ttl=len(teams)))

            for game in pools_arrange[len(teams)]:
                t_t = teams[game[0] - 1]
                b_t = teams[game[1] - 1]
                self.sql.add_games_to_leagues((t_t, b_t))
                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.games_btn.append(Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), caller=self, font_size=16))

            for btn in self.games_btn:
                self.ids.games_bar.add_widget(btn)
        elif self.mod == "f":
            foil_top = self.ids.foil_table_top
            foil_top.orientation = "vertical"
            teams.sort()

            tt = list()
            for i in teams:
                tt.append(self.sql.team_name2ID(Make_persian(i[1])))

            self.sql.create_league('f', tt)

            for items in teams:
                foil_top.add_widget(Team_table_item(no=str(items[0]), name=items[1], ttl=len(teams)))

            for game in pools_arrange[len(teams)]:
                t_t = teams[game[0] - 1]
                b_t = teams[game[1] - 1]
                self.sql.add_games_to_leagues((t_t, b_t),'f')

                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.f_games_btn.append(Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), caller=self, font_size=16))

            for btn in self.f_games_btn:
                self.ids.f_games_bar.add_widget(btn)
        elif self.mod == "s":
            sabre_top = self.ids.sabre_table_top
            sabre_top.orientation = "vertical"
            teams.sort()

            tt = list()
            for i in teams:
                tt.append(self.sql.team_name2ID(Make_persian(i[1])))

            self.sql.create_league('s', tt)

            for items in teams:
                sabre_top.add_widget(Team_table_item(no=str(items[0]), name=items[1], ttl=len(teams)))

            for game in pools_arrange[len(teams)]:
                t_t = teams[game[0] - 1]
                b_t = teams[game[1] - 1]
                self.sql.add_games_to_leagues((t_t, b_t),'s')

                txt = "{text: ^20}".format(text=Make_persian(str(t_t[0]) + "-" + t_t[1])) + \
                      '\n' + \
                      "{text: ^20}".format(text=Make_persian(str(b_t[0]) + "-" + b_t[1]))

                self.s_games_btn.append(Game_btn(name=txt, game_ids=(t_t[1], b_t[1]), caller=self, font_size=16))

            for btn in self.s_games_btn:
                self.ids.s_games_bar.add_widget(btn)

    def ppr(self, btn, mod):
        self.manager.parent.ppr(mod=mod, dt=btn)

    def name_putter(self, team_1, team_2, mod='e'):
        if mod == 'e':
            self.ids.epee_paper.name_putter(team_1, team_2)
        elif mod == 'f':
            self.ids.foil_paper.name_putter(team_1, team_2)
        elif mod == 's':
            self.ids.sabre_paper.name_putter(team_1, team_2)

    def print_table(self, mod='e'):

        pass