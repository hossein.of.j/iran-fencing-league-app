from extra_classes import *


class Menu_bar(BoxLayout):
    menues_titles = ['open League', 'new league', 'export']

    def __init__(self, *args, **kwargs):
        super(Menu_bar, self).__init__(*args, **kwargs)
        self.manager = Button(text="Manage", background_normal="", background_down="", background_color=(1, 1, 1, 1),
                              size_hint=(None, 1), width=80, color=(0, 0, 0, 1))

        self.add_widget(self.manager)

        self.manager_menu = menu(stack_pos=self.manager.pos)
        self.manager.bind(on_release=lambda x: self.manager_menu.open())

        mens = [["new League", lambda: self.new_league()],
                ["Open League", lambda: self.open_league()],
                ["Teams", lambda: self.Teams()],
                ["League Sheet", lambda: self.league()],
                ["setting", lambda: self.setting()],
                ["exit", lambda: self.ext()]]

        for men in mens:
            self.manager_menu.add_menu(men)

        self.tools = Button(text="Tools", background_normal="", background_down="", background_color=(1, 1, 1, 1),
                            size_hint=(None, 1), width=80, color=(0, 0, 0, 1))

        self.add_widget(self.tools)

        self.tools_menu = menu(stack_pos=self.tools.pos)
        self.tools.bind(on_release=lambda x: self.tools_menu.open())
        mens = [["teams result ", self.full_team_result],
                ["Team Forms", self.team_forms],
                ["export to Excel", lambda: print("not writed yet")],
                ["export to XML", lambda: print("not writed yet")],
                ["export to XML(fie)", lambda: print("not writed yet")]]

        for men in mens:
            self.tools_menu.add_menu(men)

        Clock.schedule_once(self._finish_init)

    def new_league(self):
        self.parent.new()
        self.manager_menu.dismiss()

    def open_league(self):
        self.parent.open()
        self.manager_menu.dismiss()

    def Teams(self):
        self.parent.go_teams()
        self.manager_menu.dismiss()

    def league(self):
        self.parent.go_league()
        self.manager_menu.dismiss()


    def setting(self):
        pass

    def ext(self):
        exit()

    def full_team_result(self):
        self.parent.result_teams()
        self.tools_menu.dismiss()

    def team_forms(self):
        self.parent.team_forms()
        self.tools_menu.dismiss()

    def _finish_init(self, dt):
        pass


class menu(ModalView):

    def __init__(self, stack_pos, *args, **kwargs):
        super(menu, self).__init__(*args, **kwargs)
        self.bg = BoxLayout(orientation="vertical")
        self.add_widget(self.bg)
        self.item_height = 25
        self.stack_pos = stack_pos
        self.size_hint = (None, None)
        self.width = 200

        # self.pos=(self.stack_pos[0],self.stack_pos[1]-self.height)

        with self.canvas.before:
            Color(rgb=(.8, .8, .8))
            self.bg_rec = Rectangle(size=self.size, pos=self.pos)

        self.bind(size=self.ps_update,
                  pos=self.ps_update)

        self.background = ""

    def add_menu(self, lst):
        men = Button(text=lst[0], background_normal="", background_down="", background_color=(1, 1, 1, 1),
                     color=(0, 0, 0, 1), size_hint=(1, None), height=self.item_height, pos_hint={"x": 0.1})

        men.halign = "left"
        men.bind(size=men.setter('text_size'))
        men.bind(on_release=lambda x: lst[1]())

        self.bg.add_widget(men)
        self.bg.height = len(self.bg.children) * self.item_height

    def ps_update(self, *args):
        self.height = len(self.bg.children) * self.item_height
        self.pos = (self.stack_pos[0], self.stack_pos[1] - self.height)
        self.bg_rec.size = self.size
        self.bg_rec.pos = self.pos
