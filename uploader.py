from importers import *
import requests

class uploader:
    def __init__(self, sql):
        self.url = 'http://goldensword.ir/input.php'
        self.sql = sql
        # self.sql = db.Databasecontroller(data_base_name='C:\\Users\\hossein\\Desktop\\banovan.db')

    def update_site(self):
        self.inds()
        self.teams()
        self.send_to_site()
        print("Site Updated")
        pass

    def make_str(self):
        self.string = ''

        for pl in self.e_top_3:
            self.string += pl[1] + str(";") + str(pl[2]) + str(";") + str(pl[3])+ str(";") + str(pl[4]) +'::'
        self.string += ">>>\n"
        for pl in self.f_top_3:
            self.string += pl[1] + str(";") + str(pl[2]) + str(";") + str(pl[3]) + str(";") + str(pl[4]) + '::'
        self.string += ">>>\n"
        for pl in self.s_top_3:
            self.string += pl[1] + str(";") + str(pl[2]) + str(";") + str(pl[3]) + str(";") + str(pl[4]) + '::'
        self.string += ">>>\n"
        for team in self.e_teams:
            self.string += team[1] + ";" + str(team[2]) + ";" + str(team[3]) + ";" + str(team[4]) + ";" + str(team[5]) + ";" + str(team[6]) + "::"
        self.string += ">>>\n"
        for team in self.f_teams:
            self.string += team[1] + ";" + str(team[2]) + ";" + str(team[3]) + ";" + str(team[4]) + ";" + str(
                team[5]) + ";" + str(team[6]) + "::"
        self.string += ">>>\n"
        for team in self.s_teams:
            self.string += team[1] + ";" + str(team[2]) + ";" + str(team[3]) + ";" + str(team[4]) + ";" + str(
                team[5]) + ";" + str(team[6]) + "::"
        self.string += ">>>\n"

    def send_to_site(self):
        self.make_str()
        try:
            requests.post(self.url,data={'fnt':self.string})
        except:
            pass

    def teams(self):
        e_res = self.extractor(mod='epee')[0]
        f_res = self.extractor(mod='foil')[0]
        s_res = self.extractor(mod='sabre')[0]

        self.e_teams = list()
        self.f_teams = list()
        self.s_teams = list()

        for team in e_res:
            re = self.extract_team_res(team[0])
            self.e_teams.append([team[0], team[1]] + re)
        for team in f_res:
            re = self.extract_team_res(team[0], mod='f')
            self.f_teams.append([team[0], team[1]] + re)
        for team in s_res:
            re = self.extract_team_res(team[0], mod='s')
            self.s_teams.append([team[0], team[1]] + re)

        self.e_teams.sort(key=lambda item: item[-1], reverse=True)
        self.f_teams.sort(key=lambda item: item[-1], reverse=True)
        self.s_teams.sort(key=lambda item: item[-1], reverse=True)


        pass

    def extract_team_res(self, ID, mod='e'):
        all_games = self.sql.extract_finished_games(team_id=ID, weapon=mod)
        game_played = len(all_games)
        win = len([i for i in all_games if i[1] == 'W'])
        loos = len([i for i in all_games if i[1] == 'L'])
        appsend = len([i for i in all_games if i[1] == 'A'])
        score = win * 3 + loos * 1
        return [game_played, win, loos, appsend, score]

    def inds(self):
        e_pls = self.extractor(mod='epee')[1]
        f_pls = self.extractor(mod='foil')[1]
        s_pls = self.extractor(mod='sabre')[1]
        self.e_top_3 = self.calc(e_pls)
        self.f_top_3 = self.calc(f_pls)
        self.s_top_3 = self.calc(s_pls)

    def extractor(self, mod):
        teams = self.sql.extract_all_team(weapon=mod)
        pls = list()
        lst = {'epee':[2,3,4,5,6],'foil':[7,8,9,10,11], 'sabre':[12,13,14,15,16]}
        for team in teams:
            i = [team[d] for d in lst[mod]]
            pls += i
        return teams, pls

    def calc(self, pls):
        dt = list()

        for pl in pls:
            if pl == 0 or pl == '' or pl == None : continue
            name = self.sql.Player_ID2name(pl)
            dif, gp =self.sql.Player_extract_sc(pl)
            try:
                ratio = round(dif / gp, 3)
            except ZeroDivisionError:
                ratio = 0

            dt.append([pl,name,dif,gp,ratio])

        dt.sort(key=lambda item: item[-1],reverse=True)
        for pl in dt:
            if pl[3] < 5:
                dt.remove(pl)
        dt.sort(key=lambda item: item[-1], reverse=True)

        return dt[:3]


if __name__ == "__main__":
    s = uploader(sql="")
    s.update_site()