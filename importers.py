from kivy.app import App
from kivy.graphics import Color, Line, Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.properties import StringProperty, NumericProperty,ObjectProperty
from kivy.garden.FileBrowser import FileBrowser
from kivy.uix.modalview import ModalView
import Data_base as db
from Classes import Player, Team
from kivy.uix.popup import Popup
from kivy.uix.togglebutton import ToggleButton
import random
import datetime
from kivy.uix.floatlayout import FloatLayout
from bidi.algorithm import get_display
import arabic_reshaper
import printer as PRINT


pool_5 =[(1,2),(3,4),(5,1),(2,3),(5,4),(1,3),(2,5),(4,1),(3,5),(4,2)]

pool_6 = [(1, 2), (4, 3), (6, 5), (3, 1), (2, 6), (5, 4), (1, 6), (3, 5), (4, 2), (5, 1), (6, 4), (2, 3), (1, 4),
          (5, 2), (3, 6)]
pool_7 = [(1, 4), (2, 5), (3, 6), (7, 1), (5, 4), (2, 3), (6, 7), (5, 1), (4, 3), (6, 2), (5, 7), (3, 1), (4, 6),
          (7, 2), (3, 5), (1, 6), (2, 4), (7, 3), (6, 5), (1, 2), (4, 7)]
pool_8 = [(2, 3), (1, 5), (7, 4), (6, 8), (1, 2), (3, 4), (5, 6), (8, 7), (4, 1), (5, 2), (8, 3), (6, 7), (4, 2),
          (8, 1), (7, 5), (3, 6), (2, 8), (5, 4), (6, 1), (3, 7), (4, 8), (2, 6), (3, 5), (1, 7), (4, 6), (8, 5),
          (7, 2), (1, 3)]
pool_9 = [(1, 9), (2, 8), (3, 7), (4, 6), (1, 5), (2, 9), (8, 3), (7, 4), (6, 5), (1, 2), (9, 3), (8, 4), (7, 5),
          (6, 1), (3, 2), (9, 4), (5, 8), (7, 6), (3, 1), (2, 4), (5, 9), (8, 6), (7, 1), (4, 3), (5, 2), (6, 9),
          (8, 7), (4, 1), (5, 3), (6, 2), (9, 7), (1, 8), (4, 5), (3, 6), (2, 7), (9, 8)]

pools_arrange = {5:pool_5, 6: pool_6, 7: pool_7, 8: pool_8, 9: pool_9}
