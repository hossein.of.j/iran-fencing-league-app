import sqlite3
import Classes
from bidi.algorithm import get_display
import arabic_reshaper


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


class Databasecontroller:
    def __init__(self, data_base_name="Test_Data_Base.db"):
        self.db = data_base_name
        self.conn = sqlite3.connect(self.db)
        self.curser = self.conn.cursor()
        try:
            self.curser.execute('''CREATE TABLE Players
                                                 (ID INTEGER PRIMARY KEY, name text , rh int)''')
            self.curser.execute('''CREATE TABLE Teams
                                     (ID INTEGER PRIMARY KEY, name text , ep1 int, ep2 int, ep3 int, ep4 int, ep5 int,
                                      fp1 int,fp2 int,fp3 int,fp4 int,fp5 int,
                                      sp1 int, sp2 int, sp3 int, sp4 int, sp5 int)''')

        except:
            pass

    def add_team(self, name, epee_player, foil_player, sabre_player):
        if epee_player:
            print("there is a epee player")
            if (len(epee_player) < 3) or (len(epee_player) > 5):
                print(" player count is not valid")
            else:
                c_l = len(epee_player)
                Player_id = list()
                for player in epee_player:
                    p_i = self.add_player(player.name, player.rh)
                    if not p_i:
                        return False
                    else:
                        Player_id.append(p_i[0])

                if c_l < 5:
                    for i in range(5 - c_l):
                        Player_id.append((0, "---", 0))

                self.curser.execute('SELECT * FROM Teams WHERE name = (?)', (name,))
                isit = self.curser.fetchall()
                # print(isit)
                print(Player_id[0])
                print(name)
                print(Player_id[1][0])
                if not isit:
                    self.curser.execute('INSERT INTO Teams(name, ep1, ep2, ep3, ep4, ep5) VALUES (?,?,?,?,?,?)',
                                        (name, Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0],))
                else:
                    self.curser.execute('''UPDATE Teams SET ep1=?, ep2=?, ep3=?, ep4=?, ep5=? WHERE name = (?);''',
                                        (Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0], name,))

        if foil_player:
            print("there is a foil player")
            if (len(foil_player) < 3) or (len(foil_player) > 5):
                print(" player count is not valid")
            else:
                c_l = len(foil_player)
                Player_id = list()
                for player in foil_player:
                    p_i = self.add_player(player.name, player.rh)
                    if not p_i:
                        return False
                    else:
                        Player_id.append(p_i[0])

                if c_l < 5:
                    for i in range(5 - c_l):
                        Player_id.append((0, "---", 0))

                self.curser.execute('SELECT * FROM Teams WHERE name = (?)', (name,))
                isit = self.curser.fetchall()
                # print(isit)
                print(Player_id[0])
                print(name)
                print(Player_id[1][0])
                if not isit:
                    self.curser.execute('INSERT INTO Teams(name, fp1, fp2, fp3, fp4, fp5) VALUES (?,?,?,?,?,?)',
                                        (name, Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0],))
                else:
                    self.curser.execute('''UPDATE Teams SET fp1=?, fp2=?, fp3=?, fp4=?, fp5=? WHERE name = (?);''',
                                        (Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0], name,))

        if sabre_player:
            print("there is a Sabre player")
            if (len(sabre_player) < 3) or (len(sabre_player) > 5):
                print(" player count is not valid")
            else:
                c_l = len(sabre_player)
                Player_id = list()
                for player in sabre_player:
                    p_i = self.add_player(player.name, player.rh)
                    if not p_i:
                        return False
                    else:
                        Player_id.append(p_i[0])

                if c_l < 5:
                    for i in range(5 - c_l):
                        Player_id.append((0, "---", 0))

                self.curser.execute('SELECT * FROM Teams WHERE name = (?)', (name,))
                isit = self.curser.fetchall()
                # print(isit)
                print(Player_id[0])
                print(name)
                print(Player_id[1][0])
                if not isit:
                    self.curser.execute('INSERT INTO Teams(name, sp1, sp2, sp3, sp4, sp5) VALUES (?,?,?,?,?,?)',
                                        (name, Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0],))
                else:
                    self.curser.execute('''UPDATE Teams SET sp1=?, sp2=?, sp3=?, sp4=?, sp5=? WHERE name = (?);''',
                                        (Player_id[0][0],
                                         Player_id[1][0],
                                         Player_id[2][0],
                                         Player_id[3][0],
                                         Player_id[4][0], name,))

        self.conn.commit()

    def extract_all_team(self, weapon='all'):
        if weapon == 'all':
            self.curser.execute('''SELECT * FROM Teams''')
            teams = self.curser.fetchall()
        elif weapon == 'epee':
            self.curser.execute('''SELECT * FROM Teams WHERE ep1 NOT NULL ''')
            teams = self.curser.fetchall()
        elif weapon == 'foil':
            self.curser.execute('''SELECT * FROM Teams WHERE fp1 NOT NULL''')
            teams = self.curser.fetchall()
        else:
            self.curser.execute('''SELECT * FROM Teams WHERE sp1 NOT NULL''')
            teams = self.curser.fetchall()

        return teams

    def extract_team_player(self, ID=None, name=None, mode='a'):
        if ID:
            if mode == 'e':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.ep1 = Players.ID OR 
                        Teams.ep2 = Players.ID OR 
                        Teams.ep3 = Players.ID OR 
                        Teams.ep4 = Players.ID OR 
                        Teams.ep5 = Players.ID) 
                        WHERE Teams.ID = ?''', (ID,))
            elif mode == 'f':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.fp1 = Players.ID OR 
                        Teams.fp2 = Players.ID OR 
                        Teams.fp3 = Players.ID OR 
                        Teams.fp4 = Players.ID OR 
                        Teams.fp5 = Players.ID) 
                        WHERE Teams.ID = ?''', (ID,))
            elif mode == 's':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.sp1 = Players.ID OR 
                        Teams.sp2 = Players.ID OR 
                        Teams.sp3 = Players.ID OR 
                        Teams.sp4 = Players.ID OR 
                        Teams.sp5 = Players.ID) 
                        WHERE Teams.ID = ?''', (ID,))
            else:
                res = self.extract_team_player(ID=ID, mode='e')
                res = res + self.extract_team_player(ID=ID, mode='f')
                res = res + self.extract_team_player(ID=ID, mode='s')
                return res

        elif name:
            if mode == 'e':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.ep1 = Players.ID OR 
                        Teams.ep2 = Players.ID OR 
                        Teams.ep3 = Players.ID OR 
                        Teams.ep4 = Players.ID OR 
                        Teams.ep5 = Players.ID) 
                        WHERE Teams.name = ?''', (name,))
            elif mode == 'f':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.fp1 = Players.ID OR 
                        Teams.fp2 = Players.ID OR 
                        Teams.fp3 = Players.ID OR 
                        Teams.fp4 = Players.ID OR 
                        Teams.fp5 = Players.ID) 
                        WHERE Teams.name = ?''', (name,))
            elif mode == 's':
                self.curser.execute('''SELECT Players.ID, Players.name FROM Teams INNER JOIN Players ON 
                        (Teams.sp1 = Players.ID OR 
                        Teams.sp2 = Players.ID OR 
                        Teams.sp3 = Players.ID OR 
                        Teams.sp4 = Players.ID OR 
                        Teams.sp5 = Players.ID) 
                        WHERE Teams.name = ?''', (name,))
            else:
                res = self.extract_team_player(name=name, mode='e')
                res = res + self.extract_team_player(name=name, mode='f')
                res = res + self.extract_team_player(name=name, mode='s')
                return res
        else:
            return "ERROR"
        res = self.curser.fetchall()
        return res

    def extract_league_teams_ID(self, mod='e'):
        if mod == 'e':
            self.curser.execute(
                '''SELECT Epee_Teams.ID, Teams.name, Teams.ID From Epee_Teams INNER JOIN Teams on (Teams.ID = Epee_Teams.team_id)''')
            res = self.curser.fetchall()
        elif mod == "f":
            self.curser.execute(
                '''SELECT Foil_Teams.ID, Teams.name, Teams.ID From Foil_Teams INNER JOIN Teams on (Teams.ID = Foil_Teams.team_id)''')
            res = self.curser.fetchall()
        elif mod == 's':
            self.curser.execute(
                '''SELECT Sabre_Teams.ID, Teams.name, Teams.ID From Sabre_Teams INNER JOIN Teams on (Teams.ID = Sabre_Teams.team_id)''')
            res = self.curser.fetchall()

        return res

    def team_name2ID(self, name):
        self.curser.execute('''SELECT * FROM Teams WHERE name =? ''', (name,))
        res = self.curser.fetchall()
        return res[0][0]

    def team_ID2name(self, ID):
        self.curser.execute('''SELECT * FROM Teams WHERE ID =? ''', (ID,))
        res = self.curser.fetchall()
        return res[0][1]

    def Player_name2ID(self, name):
        self.curser.execute('''SELECT * FROM Players WHERE name =? ''', (name,))
        res = self.curser.fetchall()
        return res[0][0]

    def Player_ID2name(self, ID):
        if ID == 0:
            return "-----"
        self.curser.execute('''SELECT * FROM Players WHERE ID =? ''', (ID,))
        res = self.curser.fetchall()
        return res[0][1]

    def Player_extract_sc(self, ID):
        if ID == 0:
            return "-----"
        self.curser.execute('''SELECT rh FROM Players WHERE ID =? ''', (ID,))
        res = self.curser.fetchall()
        if res[0][0] == 1: return 0,0
        dif, g_p = res[0][0].split(':')[0],res[0][0].split(':')[1]
        dif = int(dif)
        g_p = int(g_p)
        return dif, g_p

    def Player_add_sc(self,ID, dif, gp):
        o_dif, o_g_p = self.Player_extract_sc(ID)

        st = str(dif+o_dif) + ":" + str(gp+o_g_p)

        self.curser.execute('''update Players 
                                set rh = ? where ID = ?''',(st,ID,))
        self.conn.commit()

    def add_player(self, name, rh=True):
        self.curser.execute('''SELECT * FROM Players WHERE name = ? AND rh = ?''', (name, rh,))
        res = self.curser.fetchall()
        if not res:
            self.curser.execute('''INSERT INTO Players(name, rh) VALUES ( ?, ?)''', (name, rh))
            self.conn.commit()
            self.curser.execute('''SELECT * FROM Players WHERE name = ? AND rh = ?''', (name, rh,))
            res = self.curser.fetchall()
            return res
        else:

            return False

    def create_league(self, mod, teams):
        if mod == 'e':
            self.curser.execute('''CREATE TABLE Epee_Teams (ID INTEGER PRIMARY KEY,team_id int)''')
            for items in teams:
                self.curser.execute('''INSERT INTO Epee_Teams (team_id) VALUES (?)''', (items,))

            self.curser.execute('''CREATE TABLE Epee_League_games_scores
                                            (ID INTEGER PRIMARY KEY,g_id int, g1 text,g2 text, g3 text, g4 text, g5 text, g6 text, g7 text, g8 text, g9 text)''')
            self.curser.execute('''CREATE TABLE Epee_League_games_plays
                                                    (ID INTEGER PRIMARY KEY,Statuse text, team1 text, team2 text, t123 int, t456 int, g11 int,g21 int, g31 int, g41 int, g51 int, g61 int, g71 int, g81 int, g91 int,
                                                    g12 int,g22 int, g32 int, g42 int, g52 int, g62 int, g72 int, g82 int, g92 int, R1 int, R2 int)''')
        elif mod == 'f':
            self.curser.execute('''CREATE TABLE Foil_Teams (ID INTEGER PRIMARY KEY,team_id int)''')
            for items in teams:
                self.curser.execute('''INSERT INTO Foil_Teams (team_id) VALUES (?)''', (items,))

            self.curser.execute('''CREATE TABLE Foil_League_games_scores
                                                        (ID INTEGER PRIMARY KEY,g_id int, g1 text,g2 text, g3 text, g4 text, g5 text, g6 text, g7 text, g8 text, g9 text)''')
            self.curser.execute('''CREATE TABLE Foil_League_games_plays
                                                                (ID INTEGER PRIMARY KEY,Statuse text, team1 text, team2 text, t123 int, t456 int, g11 int,g21 int, g31 int, g41 int, g51 int, g61 int, g71 int, g81 int, g91 int,
                                                                g12 int,g22 int, g32 int, g42 int, g52 int, g62 int, g72 int, g82 int, g92 int, R1 int, R2 int)''')
        elif mod == "s":
            self.curser.execute('''CREATE TABLE Sabre_Teams (ID INTEGER PRIMARY KEY,team_id int)''')
            for items in teams:
                self.curser.execute('''INSERT INTO Sabre_Teams (team_id) VALUES (?)''', (items,))

            self.curser.execute('''CREATE TABLE Sabre_League_games_scores
                                                        (ID INTEGER PRIMARY KEY,g_id int, g1 text,g2 text, g3 text, g4 text, g5 text, g6 text, g7 text, g8 text, g9 text)''')
            self.curser.execute('''CREATE TABLE Sabre_League_games_plays
                                                                (ID INTEGER PRIMARY KEY,Statuse text, team1 text, team2 text, t123 int, t456 int, g11 int,g21 int, g31 int, g41 int, g51 int, g61 int, g71 int, g81 int, g91 int,
                                                                g12 int,g22 int, g32 int, g42 int, g52 int, g62 int, g72 int, g82 int, g92 int, R1 int, R2 int)''')
        self.conn.commit()

    def check_exiting_leaque(self):
        try:
            self.curser.execute('''SELECT * FROM Epee_League_games_plays''')
            e = True
        except sqlite3.OperationalError:
            e = False

        try:
            self.curser.execute('''SELECT * FROM Foil_League_games_plays''')
            f = True
        except sqlite3.OperationalError:
            f = False

        try:
            self.curser.execute('''SELECT * FROM Sabre_League_games_plays''')
            s = True
        except sqlite3.OperationalError:
            s = False
        return e, f, s

    def add_games_to_leagues(self, game, mod='e'):
        t1 = self.leag_id_to_team_id(game[0][0], mod)
        t2 = self.leag_id_to_team_id(game[1][0], mod)
        if mod == 'e':
            self.curser.execute('''INSERT INTO Epee_League_games_plays (Statuse, team1, team2) VALUES (?, ?, ?)''',
                                ("Null",
                                 t1,
                                 t2,))

        elif mod == "f":
            self.curser.execute('''INSERT INTO Foil_League_games_plays (Statuse, team1, team2) VALUES (?, ?, ?)''',
                                ("Null",
                                 t1,
                                 t2,))
        elif mod == "s":
            self.curser.execute('''INSERT INTO Sabre_League_games_plays (Statuse, team1, team2) VALUES (?, ?, ?)''',
                                ("Null",
                                 t1,
                                 t2,))
        self.conn.commit()

    def leag_id_to_team_id(self, ID, mod='e'):
        if mod == 'e':
            self.curser.execute('''select team_id from Epee_Teams where ID = ?''',(ID,))
            res = self.curser.fetchall()
        elif mod =='f':
            self.curser.execute('''select team_id from Foil_Teams where ID = ?''',(ID,))
            res = self.curser.fetchall()
        elif mod=='s':
            self.curser.execute('''select team_id from Sabre_Teams where ID = ?''',(ID,))
            res = self.curser.fetchall()

        return res[0][0]

    def extract_games(self, mod='e', ID=0):
        if mod == 'e':
            if ID == 0:
                self.curser.execute('''select E.Statuse, E.team1, E.team2, T1.name, T2.name 
                                        from Epee_League_games_plays as E 
                                        inner join Teams as T1 on T1.ID = E.team1 
                                        inner join Teams as T2 on T2.ID = E.team2''')
            else:
                self.curser.execute('''select E.team1, E.t123, E.g21, E.g31, E.g11, E.R1, E.team2, E.t456, E.g32, E.g22, E.g12, E.R2 
                                        from Epee_League_games_plays as E
                                        where E.ID = ?''', (ID,))
            res = self.curser.fetchall()

        elif mod == 'f':
            if ID == 0:
                self.curser.execute('''select E.Statuse, E.team1, E.team2, T1.name, T2.name 
                                        from Foil_League_games_plays as E 
                                        inner join Teams as T1 on T1.ID = E.team1 
                                        inner join Teams as T2 on T2.ID = E.team2''')
            else:
                self.curser.execute('''select E.team1, E.t123, E.g21, E.g31, E.g11, E.R1, E.team2, E.t456, E.g32, E.g22, E.g12, E.R2 
                                        from Foil_League_games_plays as E
                                        where E.ID = ?''', (ID,))
            res = self.curser.fetchall()

        elif mod == 's':
            if ID == 0:
                self.curser.execute('''select E.Statuse, E.team1, E.team2, T1.name, T2.name 
                                        from Sabre_League_games_plays as E 
                                        inner join Teams as T1 on T1.ID = E.team1 
                                        inner join Teams as T2 on T2.ID = E.team2''')
            else:
                self.curser.execute('''select E.team1, E.t123, E.g21, E.g31, E.g11, E.R1, E.team2, E.t456, E.g32, E.g22, E.g12, E.R2 
                                        from Sabre_League_games_plays as E
                                        where E.ID = ?''', (ID,))
            res = self.curser.fetchall()

        return res

    def extract_team_pos(self, ID, mod='e'):
        if mod == 'e':
            self.curser.execute('''SELECT ID FROM Epee_Teams WHERE team_id = ?''', (ID,))
            res = self.curser.fetchall()
        elif mod == 'f':
            self.curser.execute('''SELECT ID FROM Foil_Teams WHERE team_id = ?''', (ID,))
            res = self.curser.fetchall()
        elif mod == "s":
            self.curser.execute('''SELECT ID FROM Sabre_Teams WHERE team_id = ?''', (ID,))
            res = self.curser.fetchall()
        return res[0][0]

    def Team_name2ID(self, name):
        self.curser.execute('''SELECT ID FROM Teams WHERE name =? ''', (name,))
        res = self.curser.fetchall()
        return res[0][0]

    def update_game_plays(self, game_id, team1, team2, mod='e'):
        gi = game_id
        tm1 = team1.copy()
        tm2 = team2.copy()

        for i in range(4):
            if tm1[i + 1] == "":
                tm1[i + 1] = 0
            else:
                tm1[i + 1] = tm1[i + 1][1]  # self.Player_name2ID(team1[i + 1])

        for i in range(4):
            if tm2[i + 1] == "":
                tm2[i + 1] = 0
            else:
                tm2[i + 1] = tm2[i + 1][1]
        if mod == 'e':
            self.curser.execute('''UPDATE Epee_League_games_plays
                                    SET Statuse = ?, t123 = ?, t456 = ?, g11 = ?, g21 = ?, g31 = ?, g41 = ?, g51 = ?, g61 = ?, g71 = ?, g81 = ?, g91 = ?,
                                                    g12 = ?, g22 = ?, g32 = ?, g42 = ?, g52 = ?, g62 = ?, g72 = ?, g82 = ?, g92 = ?, R1=?, R2 = ?
                                    WHERE ID = ?''',
                                ("Running", tm1[0], tm2[0],
                             tm1[3], tm1[1], tm1[2], tm1[1], tm1[3], tm1[2], tm1[1], tm1[2], tm1[3],
                             tm2[3], tm2[2], tm2[1], tm2[3], tm2[1], tm2[2], tm2[1], tm2[3], tm2[2],
                             tm1[4], tm2[4], gi,))
        elif mod == 'f':
            self.curser.execute('''UPDATE Foil_League_games_plays
                                                                     SET Statuse = ?, t123 = ?, t456 = ?, g11 = ?, g21 = ?, g31 = ?, g41 = ?, g51 = ?, g61 = ?, g71 = ?, g81 = ?, g91 = ?,
                                                                                     g12 = ?, g22 = ?, g32 = ?, g42 = ?, g52 = ?, g62 = ?, g72 = ?, g82 = ?, g92 = ?, R1=?, R2 = ?
                                                                     WHERE ID = ?''',
                                ("Running", tm1[0], tm2[0],
                                 tm1[3], tm1[1], tm1[2], tm1[1], tm1[3], tm1[2], tm1[1], tm1[2], tm1[3],
                                 tm2[3], tm2[2], tm2[1], tm2[3], tm2[1], tm2[2], tm2[1], tm2[3], tm2[2],
                                 tm1[4], tm2[4], gi,))
        elif mod == 's':
            self.curser.execute('''UPDATE Sabre_League_games_plays
                                                SET Statuse = ?, t123 = ?, t456 = ?, g11 = ?, g21 = ?, g31 = ?, g41 = ?, g51 = ?, g61 = ?, g71 = ?, g81 = ?, g91 = ?,
                                                                g12 = ?, g22 = ?, g32 = ?, g42 = ?, g52 = ?, g62 = ?, g72 = ?, g82 = ?, g92 = ?, R1=?, R2 = ?
                                                WHERE ID = ?''',
                                ("Running", tm1[0], tm2[0],
                                 tm1[3], tm1[1], tm1[2], tm1[1], tm1[3], tm1[2], tm1[1], tm1[2], tm1[3],
                                 tm2[3], tm2[2], tm2[1], tm2[3], tm2[1], tm2[2], tm2[1], tm2[3], tm2[2],
                                 tm1[4], tm2[4], gi,))

        #
        # self.curser.execute('''UPDATE Epee_League_games_plays
        #                                 SET Statuse = ?, t123 = ?, t456 = ?
        #                                 WHERE ID = ?''',
        #                     ["Running", team1[0], team2[0],
        #                      game_id, ])
        self.conn.commit()

    def extract_game_id(self, team1, team2, mod='e'):
        t1 = self.team_name2ID(team1)
        t2 = self.team_name2ID(team2)
        if mod=='e':
            self.curser.execute('''select ID from Epee_League_games_plays where team1 = ? and team2 = ?''', (t1, t2,))
        elif mod=='f':
            self.curser.execute('''select ID from Foil_League_games_plays where team1 = ? and team2 = ?''', (t1, t2,))
        elif mod=='s':
            self.curser.execute('''select ID from Sabre_League_games_plays where team1 = ? and team2 = ?''', (t1, t2,))
        res = self.curser.fetchall()
        if not res:
            if mod=='e':
                self.curser.execute('''select ID from Epee_League_games_plays where team1 = ? and team2 = ?''',
                                    (t2, t1,))
            elif mod=='f':
                self.curser.execute('''select ID from Foil_League_games_plays where team1 = ? and team2 = ?''',
                                    (t2, t1,))
            elif mod=='s':
                self.curser.execute('''select ID from Sabre_League_games_plays where team1 = ? and team2 = ?''',
                                    (t2, t1,))

            res = self.curser.fetchall()
        return res[0][0]

    def add_game_res(self, g_id, game_scores, mod='e'):
        if mod == 'e':
            self.curser.execute(
                '''INSERT INTO Epee_League_games_scores (g_id, g1, g2, g3, g4, g5, g6, g7, g8, g9) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                (g_id, game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],))

            self.curser.execute('''UPDATE Epee_League_games_plays
                                            SET Statuse = ?
                                            WHERE ID = ?''',
                                ("Finished", g_id,))
        elif mod == 'f':
            self.curser.execute(
                '''INSERT INTO Foil_League_games_scores (g_id, g1, g2, g3, g4, g5, g6, g7, g8, g9) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                (g_id, game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],))

            self.curser.execute('''UPDATE Foil_League_games_plays
                                                        SET Statuse = ?
                                                        WHERE ID = ?''',
                                ("Finished", g_id,))
        elif mod == 's':
            self.curser.execute(
                '''INSERT INTO Sabre_League_games_scores (g_id, g1, g2, g3, g4, g5, g6, g7, g8, g9) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                (g_id, game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],))

            self.curser.execute('''UPDATE Sabre_League_games_plays
                                                        SET Statuse = ?
                                                        WHERE ID = ?''',
                                ("Finished", g_id,))

        self.conn.commit()

    def update_game_res(self, g_id, game_scores, mod='e'):
        if mod == 'e':
            self.curser.execute(
                '''UPDATE Epee_League_games_scores SET g1=?, g2=?, g3=?, g4=?, g5=?, g6=?, g7=?, g8=?, g9=? where g_id= ?''',
                (game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],g_id, ))

        elif mod == 'f':
            self.curser.execute(
                '''UPDATE Foil_League_games_scores SET g1=?, g2=?, g3=?, g4=?, g5=?, g6=?, g7=?, g8=?, g9=? where g_id= ?''',
                (game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],g_id, ))

        elif mod == 's':
            self.curser.execute(
                '''UPDATE Sabre_League_games_scores SET g1=?, g2=?, g3=?, g4=?, g5=?, g6=?, g7=?, g8=?, g9=? where g_id= ?''',
                (game_scores[0], game_scores[1], game_scores[2],
                 game_scores[3], game_scores[4], game_scores[5],
                 game_scores[6], game_scores[7], game_scores[8],g_id, ))


        self.conn.commit()

    def extract_finished_games(self, team_id, weapon="e"):
        if weapon == "e":
            self.curser.execute('''select ID from Epee_League_games_plays where Statuse="Finished" and (team1=? or team2= ?)''',(team_id, team_id,))
            res =self.curser.fetchall()
            games= list()
            for dt in res:
                g_res = self.extract_winner(dt[0])
                if (g_res[2] == (45,0)) and (g_res[1] == team_id):
                    st = "A"
                elif g_res[1] == team_id:
                    st = "L"
                else:
                    st = "W"
                games.append([dt[0],st])
            return games
            pass
        elif weapon == 'f':
            self.curser.execute('''select ID from Foil_League_games_plays where Statuse="Finished" and (team1=? or team2= ?)''',(team_id, team_id,))
            res =self.curser.fetchall()
            games= list()
            for dt in res:
                g_res = self.extract_winner(dt[0],mod='f')
                if (g_res[2] == (45,0)) and (g_res[1] == team_id):
                    st = "A"
                elif g_res[1] == team_id:
                    st = "L"
                else:
                    st = "W"
                games.append([dt[0],st])
            return games
            pass
        elif weapon == 's':
            self.curser.execute('''select ID from Sabre_League_games_plays where Statuse="Finished" and (team1=? or team2= ?)''',(team_id, team_id,))
            res =self.curser.fetchall()
            games= list()
            for dt in res:
                g_res = self.extract_winner(dt[0],mod='s')
                if (g_res[2] == (45,0)) and (g_res[1] == team_id):
                    st = "A"
                elif g_res[1] == team_id:
                    st = "L"
                else:
                    st = "W"
                games.append([dt[0],st])
            return games
            pass

        return

    def extract_winner(self, g_id, mod='e'):
        if mod=='e':
            self.curser.execute('''SELECT g9 FROM Epee_League_games_scores WHERE g_id = ?''', (g_id,))
        elif mod == 'f':
            self.curser.execute('''SELECT g9 FROM Foil_League_games_scores WHERE g_id = ?''', (g_id,))
        elif mod == 's':
            self.curser.execute('''SELECT g9 FROM Sabre_League_games_scores WHERE g_id = ?''', (g_id,))

        res = self.curser.fetchall()
        res = res[0][0].split("-")
        r1, r2 = int(res[0]), int(res[1])
        if mod == 'e':
            self.curser.execute('''SELECT t123, t456 FROM Epee_League_games_plays WHERE ID = ?''', (g_id,))
        elif mod == 'f':
            self.curser.execute('''SELECT t123, t456 FROM Foil_League_games_plays WHERE ID = ?''', (g_id,))
        else:
            self.curser.execute('''SELECT t123, t456 FROM Sabre_League_games_plays WHERE ID = ?''', (g_id,))

        res2 = self.curser.fetchall()
        res2 = res2[0]

        res2 = (self.team_name2ID(res2[0]),self.team_name2ID(res2[1]))

        if r1 > r2:
            return res2[0], res2[1], (r1, r2)
        else:
            return res2[1], res2[0], (r2, r1)

    def check_game_status(self, g_id, mod='e'):
        g_id =self.extract_game_id(Make_persian(g_id[0]),Make_persian(g_id[1]),mod)
        if mod=='e':
            self.curser.execute('''SELECT Statuse FROM Epee_League_games_plays WHERE ID = ?''', (g_id,))
        elif mod == 'f':
            self.curser.execute('''SELECT Statuse FROM Foil_League_games_plays WHERE ID = ?''', (g_id,))
        elif mod == 's':
            self.curser.execute('''SELECT Statuse FROM Sabre_League_games_plays WHERE ID = ?''', (g_id,))

        res = self.curser.fetchall()
        return res[0][0]

    def extract_game_result(self, g_id, mod='e'):
        g_id = self.extract_game_id(Make_persian(g_id[0]), Make_persian(g_id[1]), mod)
        if mod == 'e':
            self.curser.execute('''SELECT * FROM Epee_League_games_scores WHERE g_id = ?''', (g_id,))
        elif mod == 'f':
            self.curser.execute('''SELECT * FROM Foil_League_games_scores WHERE g_id = ?''', (g_id,))
        elif mod == 's':
            self.curser.execute('''SELECT * FROM Sabre_League_games_scores WHERE g_id = ?''', (g_id,))

        res =self.curser.fetchall()

        return res[0]

    def update_player_name(self, name, ID):
        self.curser.execute('''UPDATE Players SET name=? WHERE ID=?''',(name,ID,))
        self.conn.commit()

    def update_team_players(self, team_name,p_ids, mod='e'):
        team_id = self.team_name2ID(team_name)
        if mod == 'e':
            self.curser.execute('''update Teams set ep1=?,ep2=?,ep3=?,ep4=?,ep5=? where ID=?''',
                                (p_ids[0],p_ids[1],
                                 p_ids[2],
                                 p_ids[3],
                                 p_ids[4],team_id,))
        self.conn.commit()


if __name__ == "__main__":
    ss = Databasecontroller(data_base_name='C:\\Users\\hossein\\Desktop\\banovan.db')
    print(ss.extract_game_result(3))
