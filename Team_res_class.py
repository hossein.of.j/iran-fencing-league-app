from extra_classes import *
import xlsx as XL


class TeamItemRes(BoxLayout):
    score = NumericProperty()
    appsend = NumericProperty()
    loos = NumericProperty()
    win = NumericProperty()
    played_games = NumericProperty()
    rank = NumericProperty()
    club_name = StringProperty()

    pass


class Team_results(Screen):
    def __init__(self, *args, **kwargs):
        super(Team_results, self).__init__(*args, **kwargs)
        Clock.schedule_once(lambda dt: self._finish_init())

    def _finish_init(self):
        self.sql = self.manager.sql
        # self.sql = Data_base.Databasecontroller()

    def on_enter(self, *args):
        self.extract_teams()

    def extract_teams(self):
        e_res = self.sql.extract_all_team(weapon="epee")
        f_res = self.sql.extract_all_team(weapon="foil")
        s_res = self.sql.extract_all_team(weapon="sabre")
        e_teams = list()
        f_teams = list()
        s_teams = list()

        for team in e_res:
            re = self.extract_team_res(team[0])
            e_teams.append([team[0], team[1]] + re)
        for team in f_res:
            re = self.extract_team_res(team[0], mod='f')
            f_teams.append([team[0], team[1]] + re)
        for team in s_res:
            re = self.extract_team_res(team[0],mod='s')
            s_teams.append([team[0], team[1]] + re)

        e_teams.sort(key=lambda item: item[-1], reverse=True)
        f_teams.sort(key=lambda item: item[-1], reverse=True)
        s_teams.sort(key=lambda item: item[-1], reverse=True)

        self.ids["epee_team_list"].clear_widgets()
        self.ids["foil_team_list"].clear_widgets()
        self.ids["sabre_team_list"].clear_widgets()

        self.e_teams = e_teams
        self.f_teams = f_teams
        self.s_teams = s_teams

        teams = e_teams

        for i in range(len(teams)):
            t = TeamItemRes()
            t.score = teams[i][6]
            t.appsend = teams[i][5]
            t.loos = teams[i][4]
            t.win = teams[i][3]
            t.played_games = teams[i][2]
            t.club_name = teams[i][1]
            t.rank = i + 1
            self.ids["epee_team_list"].add_widget(t)

        teams = f_teams
        for i in range(len(teams)):
            t = TeamItemRes()
            t.score = teams[i][6]
            t.appsend = teams[i][5]
            t.loos = teams[i][4]
            t.win = teams[i][3]
            t.played_games = teams[i][2]
            t.club_name = teams[i][1]
            t.rank = i + 1
            self.ids["foil_team_list"].add_widget(t)

        teams = s_teams
        for i in range(len(teams)):
            t = TeamItemRes()
            t.score = teams[i][6]
            t.appsend = teams[i][5]
            t.loos = teams[i][4]
            t.win = teams[i][3]
            t.played_games = teams[i][2]
            t.club_name = teams[i][1]
            t.rank = i + 1
            self.ids["sabre_team_list"].add_widget(t)

    def extract_team_res(self, ID, mod='e'):
        all_games = self.sql.extract_finished_games(team_id=ID, weapon=mod)
        game_played = len(all_games)
        win = len([i for i in all_games if i[1] == 'W'])
        loos = len([i for i in all_games if i[1] == 'L'])
        appsend = len([i for i in all_games if i[1] == 'A'])
        score = win * 3 + loos * 1
        return [game_played, win, loos, appsend, score]

    def excel(self):
        XL.teams_res(self.e_teams,self.f_teams,self.s_teams)

    def total(self):
        pop = Popup(title="", content=Label_f(text="Not implemented yet!!"), size_hint=(.3,.8))
        pop.open()

    def refresh(self):
        self._finish_init()

