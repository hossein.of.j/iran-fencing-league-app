from Leage_screen_class import League_screen
from Teams_screen_class import Teams_screen
from Team_res_class import *
from menu_classes import *
from Team_forms_class import Team_forms_screen

import gc

Window.maximize()
Window.clearcolor = (1, 1, 1, 1)


class Main_screen(BoxLayout):

    def __init__(self, *args, **kwargs):
        super(Main_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(lambda dt: self._finish_init())
        self.file = "Nothing"
        try:
            f = open("last.last",'r')
            dt = f.read()
            self.dt = dt
            f.close()
        except FileNotFoundError:
            self.dt = "Temp.db"
        try:
            self.sql = db.Databasecontroller(data_base_name=self.dt)
        except:
            self.sql = db.Databasecontroller(data_base_name="Temp.db")

        self.FB_pop = None
        Clock.schedule_interval(self.free_memory, 10)

    def free_memory(self, dt):
        gc.collect()

    def _finish_init(self):
        self.screenmanager = self.ids.scmngr
        # self.screenmanager.current = "TeamForms"

    def new(self):
        format_acceptable = ["*.db"]
        FB = FileBrowser()
        FB.filters = format_acceptable

        FB.cancel_string = "Close"
        FB.select_string = "Save"

        FB.bind(on_success=self._fb_submit,
                on_canceled=self._fb_close)

        self.FB_pop = Popup(title="File Selection", content=FB, auto_dismiss=False,
                            size_hint=(.6, .9), pos_hint={"center_x": .5, "center_y": .5})
        self.file = "new"
        self.FB_pop.open()

    def open(self):
        format_acceptable = ["*.db"]
        FB = FileBrowser()
        FB.filters = format_acceptable

        FB.cancel_string = "Close"
        FB.select_string = "Open"

        FB.bind(on_success=self._fb_submit,
                on_canceled=self._fb_close)

        self.FB_pop = Popup(title="File Selection", content=FB, auto_dismiss=False,
                            size_hint=(.6, .9), pos_hint={"center_x": .5, "center_y": .5})
        self.file = "open"
        self.FB_pop.open()

    def _fb_close(self, instance):
        self.FB_pop.dismiss()

    def _fb_submit(self, instance):
        if instance.selection and self.file == 'open':
            self.db_addr = instance.selection[0]
            f = open("last.last", 'w')
            f.write(self.db_addr)
            f.close()

            if self.db_addr.split(".")[-1] == "db":
                # self.FB_pop.dismiss()
                self.close_pops()

            else:
                pass

        elif self.file == 'new':
            bx = BoxLayout(orientation="vertical", spacing=5, padding=20)
            self.name = TextInput(multiline=False, write_tab=False)
            self.btn = Button(text="Save")
            self.btn.bind(on_release=self.close_pops)
            bx.add_widget(self.name)
            bx.add_widget(self.btn)
            self.name_pop = Popup(title="League name", content=bx, size_hint=(None, None), size=(350, 200))
            self.name_pop.open()
            self.db_path = instance.path

        else:
            pass

    def close_pops(self, *args):
        if self.file == "new":
            if self.name.text:
                self.FB_pop.dismiss()
                self.name_pop.dismiss()
                self.db_name = self.name.text
                self.db_addr = self.db_path + "\\" + self.db_name + ".db"
        else:
            self.FB_pop.dismiss()

        self.dt = self.db_addr
        self.sql = db.Databasecontroller(data_base_name=self.db_addr)

        self.refresh()

    def refresh(self):
        self.screenmanager.refresh()
        self.screenmanager.screens[0].refresh()
        self.screenmanager.screens[1].refresh()
        self.screenmanager.screens[2].refresh()
        self.screenmanager.screens[3].refresh()

        pass

    def set_game_winner(self, winner, looser, score, mod):
        self.screenmanager.screens[0].set_game_winner(winner, looser, score, mod)
        pass

    def ppr(self, mod, dt):
        self.team_select_pop = Team_select_popup(title="Select Pop", caller=self, btn=dt, game_name=dt.game_id, mod=mod,
                                                 size_hint=(0.6, .95),
                                                 auto_dismiss=False)
        self.team_select_pop.open()

    def result_teams(self):
        self.screenmanager.current = "Team_results"

    def team_forms(self):
        self.screenmanager.current = "TeamForms"

    def go_teams(self):
        self.screenmanager.current = "TeamsScreen"

    def go_league(self):
        self.screenmanager.current = "LeagueScreen"


class Scr_Manager(ScreenManager):

    def __init__(self, *args, **kwargs):
        super(Scr_Manager, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.sql = self.parent.sql

    def refresh(self):
        self.sql = self.parent.sql



class mainApp(App):
    base = ObjectProperty()

    def build(self):
        self.base = Main_screen()
        return self.base


root = mainApp()
root.run()
